import 'package:budget_planner_app/model/category_actions.dart';
import 'package:budget_planner_app/model/currency.dart';
import 'package:budget_planner_app/provider_notify/actions_change_notifyer_controller.dart';
import 'package:budget_planner_app/provider_notify/currency_change_notifyer_controller.dart';
import 'package:budget_planner_app/utils/app_colors.dart';
import 'package:budget_planner_app/utils/size_config.dart';
import 'package:budget_planner_app/widgets/item_listview.dart';
import 'package:budget_planner_app/widgets/my_searchview.dart';
import 'package:budget_planner_app/widgets/my_text.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:grouped_list/grouped_list.dart';
import 'package:provider/provider.dart';

class ActionsScreen extends StatefulWidget {
  const ActionsScreen({Key? key}) : super(key: key);

  @override
  _ActionsScreenState createState() => _ActionsScreenState();
}

class _ActionsScreenState extends State<ActionsScreen> {
  late TextEditingController _searchController;
  List<CategoryActions> _listAction = [];
  List<Currency> _listCurrency = [];

  @override
  void initState() {
    _searchController = TextEditingController();
    super.initState();
  }

  @override
  void dispose() {
    _searchController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    _listCurrency = Provider.of<CurrencyChangeNotifierController>(context).listCurrency;
    return Scaffold(
      backgroundColor: AppColors.WHITE,
      appBar: AppBar(
        backgroundColor: AppColors.WHITE,
        elevation: 0,
        title: MyText(
          title: 'Actions',
        ),
        centerTitle: true,
        iconTheme: IconThemeData(color: AppColors.BG_BUTTON),
      ),
      body: Padding(
        padding: EdgeInsets.symmetric(horizontal: SizeConfig.scaleWidth(20)),
        child: Column(
          children: [
            SizedBox(height: SizeConfig.scaleHeight(35)),
            MySearchView(
              searchViewController: _searchController,
              bg_search: AppColors.BG_PROGRESS,
              onChange: (String char) {
                Provider.of<ActionsChangeNotifierController>(context,
                        listen: false)
                    .search(char);
              },
            ),
            SizedBox(height: SizeConfig.scaleHeight(15)),
            Expanded(
              child: Consumer<ActionsChangeNotifierController>(
                builder: (
                  BuildContext context,
                  ActionsChangeNotifierController value,
                  Widget? child,
                ) {
                  return value.listActionSearch.length == 0
                      ? Center(
                          child: Column(
                            mainAxisSize: MainAxisSize.min,
                            children: [
                              Icon(
                                Icons.error_outline,
                                size: 80,
                                color: AppColors.BG_BUTTON,
                              ),
                              SizedBox(height: SizeConfig.scaleHeight(16)),
                              MyText(
                                  title: AppLocalizations.of(context)!.noData),
                            ],
                          ),
                        )
                      : GroupedListView<CategoryActions, String>(
                          elements: value.listActionSearch,
                          groupBy: (CategoryActions element) => element.date,
                          groupSeparatorBuilder: (String groupByValue) {
                            return MyText(
                              title: groupByValue,
                              height: 2,
                              fontSize: 12,
                              color: AppColors.TEXT_NOTE,
                            );
                          },
                          itemBuilder: (context, CategoryActions element) {
                            return ItemListview(
                              context: context,
                              actions: element,
                              listCurrency: _listCurrency,
                            );
                          },
                          itemComparator:
                              (CategoryActions item1, CategoryActions item2) {
                            return item1.date.compareTo(item2.date);
                          },
                          useStickyGroupSeparators: true,
                          floatingHeader: true,
                          order: GroupedListOrder.DESC,
                          separator: Divider(),
                        );
                },
              ),
            ),
          ],
        ),
      ),
    );
  }
}
