import 'package:budget_planner_app/model/tips.dart';
import 'package:budget_planner_app/utils/app_colors.dart';
import 'package:budget_planner_app/utils/size_config.dart';
import 'package:budget_planner_app/widgets/my_text.dart';
import 'package:flutter/material.dart';
import 'package:share/share.dart';

class DetailsTipsScreen extends StatelessWidget {
  Tipss tips;

  DetailsTipsScreen(this.tips);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      extendBodyBehindAppBar: true,
      appBar: AppBar(
        elevation: 0,
        backgroundColor: Colors.transparent,
        iconTheme: IconThemeData(color: AppColors.BG_BUTTON),
        actionsIconTheme: IconThemeData(color: AppColors.BG_BUTTON),
        actions: [
          IconButton(
            onPressed: () {
              Share.share(tips.title, subject: tips.subject);
            },
            icon: Icon(Icons.ios_share, color: AppColors.BG_BUTTON),
          ),
        ],
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            SizedBox(height: SizeConfig.scaleHeight(40)),
            Image.asset(
              tips.image,
              height: SizeConfig.scaleHeight(348),
              width: double.infinity,
              // fit: BoxFit.fill,
            ),
            SizedBox(height: SizeConfig.scaleHeight(30)),
            Padding(
              padding: EdgeInsets.symmetric(
                horizontal: SizeConfig.scaleWidth(26),
              ),
              child: MyText(title: tips.title),
            ),
            SizedBox(height: SizeConfig.scaleHeight(11)),
            Padding(
              padding: EdgeInsets.symmetric(
                horizontal: SizeConfig.scaleWidth(26),
              ),
              child: MyText(
                title: tips.subject,
                fontSize: 16,
                fontWeight: FontWeight.w400,
                color: AppColors.GRAY,
                height: 1.6,
                textAlign: TextAlign.start,
              ),
            ),
            SizedBox(height: SizeConfig.scaleHeight(70)),
          ],
        ),
      ),
    );
  }
}
