import 'package:budget_planner_app/storage/pref/shared_pref_controller.dart';
import 'package:budget_planner_app/utils/app_colors.dart';
import 'package:budget_planner_app/utils/size_config.dart';
import 'package:budget_planner_app/widgets/my_contianer_logo.dart';
import 'package:budget_planner_app/widgets/my_text.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class LaunchScreen extends StatefulWidget {
  @override
  _LaunchScreenState createState() => _LaunchScreenState();
}

class _LaunchScreenState extends State<LaunchScreen> {
  @override
  void initState() {
    Future.delayed(Duration(seconds: 3), () {
      if (SharedPrefController().getUser().id! > -1)
        Navigator.pushReplacementNamed(context, '/main_bottom_navigation_screen');
      else
        Navigator.pushReplacementNamed(context, '/page_view_screen');
    });

    super.initState();
  }

  @override
  void didChangeDependencies() {
    SizeConfig().init(context);
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.BG_SCREEN_COLOR,
      body: Center(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            MyContianerLogo(
              width: 170,
              height: SizeConfig.scaleHeight(170),
              child: Image.asset('images/logo.png'),
              borderRadius: 45,
              paddingHorizontal: 42,
              paddingVertical: 24,
            ),
            SizedBox(height: SizeConfig.scaleHeight(29)),
            MyText(
              title: AppLocalizations.of(context)!.nameApp,
              fontSize: 24,
              fontWeight: FontWeight.w800,
            ),
          ],
        ),
      ),
    );
  }
}
