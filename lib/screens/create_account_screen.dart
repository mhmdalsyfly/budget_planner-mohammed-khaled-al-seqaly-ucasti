import 'package:budget_planner_app/helpers/show_snakbar.dart';
import 'package:budget_planner_app/model/currency.dart';
import 'package:budget_planner_app/model/user.dart';
import 'package:budget_planner_app/provider_notify/currency_change_notifyer_controller.dart';
import 'package:budget_planner_app/provider_notify/user_change_notifyer_controller.dart';
import 'package:budget_planner_app/screens/pin_code_screen.dart';
import 'package:budget_planner_app/storage/pref/shared_pref_controller.dart';
import 'package:budget_planner_app/utils/app_colors.dart';
import 'package:budget_planner_app/utils/size_config.dart';
import 'package:budget_planner_app/widgets/my_contianer_logo.dart';
import 'package:budget_planner_app/widgets/my_text.dart';
import 'package:budget_planner_app/widgets/my_text_button.dart';
import 'package:budget_planner_app/widgets/my_textfield2.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:provider/provider.dart';

class CreateAccountScreen extends StatefulWidget {
  const CreateAccountScreen({Key? key}) : super(key: key);

  @override
  _CreateAccountScreenState createState() => _CreateAccountScreenState();
}

class _CreateAccountScreenState extends State<CreateAccountScreen> {
  late TextEditingController _nameTextEditing;
  late TextEditingController _emailTextEditing;
  late TextEditingController _currencyNumEditing;
  late TextEditingController _dailyLimitNumEditing;
  late TapGestureRecognizer _onTapGestureRecognizer;

  Color bgButtonCreate = AppColors.BG_BUTTON_GRAY;

  String pinCode = '';
  int result = -1;
  List<User> _listUsers = [];

  @override
  void initState() {
    _nameTextEditing = TextEditingController();
    _emailTextEditing = TextEditingController();
    _currencyNumEditing = TextEditingController();
    _dailyLimitNumEditing = TextEditingController();

    super.initState();
  }

  @override
  void didChangeDependencies() {
    SizeConfig().init(context);
    super.didChangeDependencies();
    _listUsers = Provider.of<UserChangeNotifierController>(context).listUsers;
  }

  void navigationToRegister() {
    // TODO:: GO TO NEXT SCREEN
  }

  @override
  void dispose() {
    _nameTextEditing.dispose();
    _emailTextEditing.dispose();
    _currencyNumEditing.dispose();
    _dailyLimitNumEditing.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Container(
          child: Scaffold(
            backgroundColor: AppColors.BG_SCREEN_COLOR,
            appBar: AppBar(
              backgroundColor: AppColors.WHITE,
              elevation: 0,
              iconTheme: IconThemeData(color: AppColors.BG_BUTTON),
            ),
            body: Padding(
              padding: EdgeInsets.symmetric(
                horizontal: SizeConfig.scaleWidth(20),
              ),
              child: SingleChildScrollView(
                child: Column(
                  children: [
                    MyContianerLogo(
                      height: SizeConfig.scaleHeight(112),
                      child: Image.asset(
                        'images/icon_wallet.png',
                      ),
                    ),
                    SizedBox(height: SizeConfig.scaleHeight(13)),
                    MyText(title: AppLocalizations.of(context)!.getStarted),
                    SizedBox(height: SizeConfig.scaleHeight(11)),
                    MyText(
                      title: AppLocalizations.of(context)!.aminuteToStart,
                      fontSize: 15,
                      fontWeight: FontWeight.w400,
                      color: AppColors.GRAY,
                    ),
                    SizedBox(height: SizeConfig.scaleHeight(21)),
                    MyContianerLogo(
                      borderRadius: 8,
                      xOffset: 0,
                      yOffset: 3,
                      blur: 6,
                      shadow: AppColors.BLACK.withOpacity(.16),
                      paddingHorizontal: 17,
                      paddingVertical: 0,
                      width: double.infinity,
                      child: Column(
                        children: [
                          MyTextField2(
                            title: AppLocalizations.of(context)!.name,
                            subTitle: AppLocalizations.of(context)!.none,
                            controller: _nameTextEditing,
                            onTap: () {},
                            onChange: (String value) {
                              if (_checkData()) {
                                bgButtonCreate = AppColors.BG_BUTTON;
                              } else {
                                bgButtonCreate = AppColors.BG_BUTTON_GRAY;
                              }
                            },
                          ),
                          MyTextField2(
                            title: AppLocalizations.of(context)!.email,
                            subTitle: AppLocalizations.of(context)!.none,
                            controller: _emailTextEditing,
                            textInputType: TextInputType.emailAddress,
                            onTap: () {},
                            onChange: (String value) {
                              if (_checkData()) {
                                bgButtonCreate = AppColors.BG_BUTTON;
                              } else {
                                bgButtonCreate = AppColors.BG_BUTTON_GRAY;
                              }
                            },
                          ),
                          MyTextField2(
                            title: AppLocalizations.of(context)!.currency,
                            subTitle: AppLocalizations.of(context)!.dollar,
                            controller: _currencyNumEditing,
                            textInputType: TextInputType.number,
                            check: true,
                            enabled: false,
                            onTap: () {
                              _getDataFromBackCurrencyScreen();
                            },
                          ),
                          MyTextField2(
                            title: AppLocalizations.of(context)!.dailyLimit,
                            subTitle: AppLocalizations.of(context)!.ust,
                            controller: _dailyLimitNumEditing,
                            textInputType: TextInputType.number,
                            onTap: () {},
                            onChange: (String value) {
                              if (_checkData()) {
                                bgButtonCreate = AppColors.BG_BUTTON;
                              } else {
                                bgButtonCreate = AppColors.BG_BUTTON_GRAY;
                              }
                            },
                          ),
                          Container(
                            alignment: AlignmentDirectional.centerStart,
                            child: MyTextButton(
                              title: AppLocalizations.of(context)!.setPin,
                              check: false,
                              onPreesed: () {
                                _getPinCodeFromPinCodeScreen();
                              },
                              textColor: AppColors.Label_TextFiled,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
        ),
        Positioned(
          bottom: SizeConfig.scaleHeight(80),
          right: SizeConfig.scaleWidth(20),
          left: SizeConfig.scaleWidth(20),
          child: MyTextButton(
            title: AppLocalizations.of(context)!.createAccount,
            checkBgColor: bgButtonCreate,
            onPreesed: () {
              _performCreateAccount();
            },
          ),
        ),
      ],
    );
  }

  void _getDataFromBackCurrencyScreen() async {
    result =
        await Navigator.pushNamed(context, '/chose_currency_screen') as int;
    _choseCurrency(result);

    if (_checkData()) {
      bgButtonCreate = AppColors.BG_BUTTON;
    } else {
      bgButtonCreate = AppColors.BG_BUTTON_GRAY;
    }
    setState(() {});
  }

  _choseCurrency(int current) {
    switch (current) {
      case 0:
        _currencyNumEditing.text = _getCurrencies(0);
        break;
      case 1:
        _currencyNumEditing.text = _getCurrencies(1);
        break;
      case 2:
        _currencyNumEditing.text = _getCurrencies(2);
        break;
    }
  }
  String _getCurrencies(int idCurrency) {
    var currencies =
        Provider.of<CurrencyChangeNotifierController>(context, listen: false)
            .listCurrency;

    if (currencies.isNotEmpty) {
      Currency object = currencies[idCurrency];
      if (SharedPrefController().getValueLang == 'en') {
        return object.nameEn;
      } else {
        return object.nameAr;
      }
    }
    return '';
  }

  void _getPinCodeFromPinCodeScreen() async {
    pinCode = await Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => PinCodeScreen()),
    );
    if (_checkData()) {
      bgButtonCreate = AppColors.BG_BUTTON;
    } else {
      bgButtonCreate = AppColors.BG_BUTTON_GRAY;
    }
    setState(() {});
  }

  User get user {
    return User(
      name: _nameTextEditing.text.trim(),
      currencyId: result,
      dayLimit: int.parse(_dailyLimitNumEditing.text.trim()),
      email: _emailTextEditing.text.trim(),
      pin: int.parse(pinCode.trim()),
    );
  }

  bool _checkData() {
    if (_nameTextEditing.text.trim().isNotEmpty &&
        _emailTextEditing.text.trim().isNotEmpty &&
        _currencyNumEditing.text.trim().isNotEmpty &&
        _dailyLimitNumEditing.text.trim().isNotEmpty &&
        pinCode.length == 4) {
      return true;
    }
    return false;
  }

  _performCreateAccount() {
    print(result);
    if (_checkData()) {
      //TODO:: NAVIGATOR TO CREATED ACCOUNT SCREEN
      _saveDataUser();
    } else {
      Helpers.showSnackBar(
        context,
        message: AppLocalizations.of(context)!.messagePinCode,
        error: true,
      );
    }
  }

  _saveDataUser() async {
    if (_checkDuplicateEmail()) {
      Helpers.showSnackBar(
        context,
        message: AppLocalizations.of(context)!.emailDuplicate,
        error: true,
      );
      return;
    }

    int createdAccount =
        await Provider.of<UserChangeNotifierController>(context, listen: false)
            .create(user);

    bool created = createdAccount > 0;

    Helpers.showSnackBar(
      context,
      message: created
          ? AppLocalizations.of(context)!.messageSignUpSuccess
          : AppLocalizations.of(context)!.messageFoundProblem,
      error: !created ? true : false,
    );

    if (created) {
      Provider.of<UserChangeNotifierController>(context,listen: false).setUser(user);
      _navigatorCreatedAccountScreen();
    }
  }

  bool _checkDuplicateEmail() {
    for (int i = 0; i < _listUsers.length; i++) {
      if (_listUsers[i].email == _emailTextEditing.text.trim()) {
        return true;
      }
    }
    return false;
  }

  void _navigatorCreatedAccountScreen() {
    Navigator.pushNamedAndRemoveUntil(
      context,
      '/created_account_screen',
      (route) => false,
    );
  }
}
