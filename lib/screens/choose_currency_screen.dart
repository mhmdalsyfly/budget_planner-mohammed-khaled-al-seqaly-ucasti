import 'package:budget_planner_app/model/currency.dart';
import 'package:budget_planner_app/provider_notify/currency_change_notifyer_controller.dart';
import 'package:budget_planner_app/storage/pref/shared_pref_controller.dart';
import 'package:budget_planner_app/utils/app_colors.dart';
import 'package:budget_planner_app/utils/size_config.dart';
import 'package:budget_planner_app/widgets/item_currency.dart';
import 'package:budget_planner_app/widgets/my_contianer_logo.dart';
import 'package:budget_planner_app/widgets/my_text.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:provider/provider.dart';

class ChoseCurrencyScreen extends StatefulWidget {
  @override
  _ChoseCurrencyScreenState createState() => _ChoseCurrencyScreenState();
}

class _ChoseCurrencyScreenState extends State<ChoseCurrencyScreen> {
  bool item1 = false;
  bool item2 = false;
  bool item3 = false;
  var currencies;

  @override
  void initState() {
    super.initState();
  }

  @override
  void didChangeDependencies() {
    Provider.of<CurrencyChangeNotifierController>(context, listen: false)
        .readCurrency();
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    currencies =
        Provider.of<CurrencyChangeNotifierController>(context, listen: false)
            .listCurrency;
    return Scaffold(
      backgroundColor: AppColors.WHITE,
      appBar: AppBar(
        elevation: 0,
        backgroundColor: AppColors.WHITE,
        centerTitle: true,
        title: MyText(title: AppLocalizations.of(context)!.currency),
        iconTheme: IconThemeData(color: AppColors.BG_BUTTON),
      ),
      body: Padding(
        padding: EdgeInsets.symmetric(horizontal: SizeConfig.scaleWidth(20)),
        child: Column(
          children: [
            MyContianerLogo(
              marginTop: 31,
              width: double.infinity,
              paddingVertical: 27,
              paddingHorizontal: 15,
              borderRadius: 8,
              child: Column(
                children: [
                  ItemCurrency(
                    title: _getCurrencies(0),
                    enabled: item1,
                    image: 'images/palestine.png',
                    onTap: () {
                      getEnabled('shikle');
                      setState(
                        () {
                          Navigator.pop(context, 0);
                        },
                      );
                    },
                  ),
                  SizedBox(height: SizeConfig.scaleHeight(20)),
                  ItemCurrency(
                    title: _getCurrencies(1),
                    enabled: item2,
                    image: 'images/dollar.png',
                    onTap: () {
                      getEnabled('dollar');
                      setState(() {
                        Navigator.pop(context, 1);
                      });
                    },
                  ),
                  SizedBox(height: SizeConfig.scaleHeight(20)),
                  ItemCurrency(
                    title: _getCurrencies(2),
                    enabled: item3,
                    image: 'images/jordan.png',
                    onTap: () {
                      getEnabled('JOD');
                      setState(
                        () {
                          Navigator.pop(context, 2);
                        },
                      );
                    },
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  getEnabled(String currency) {
    if (currency == 'shikle') {
      item1 = true;
      item2 = false;
      item3 = false;
    } else if (currency == 'dollar') {
      item1 = false;
      item2 = true;
      item3 = false;
    } else if (currency == 'JOD') {
      item1 = false;
      item2 = false;
      item3 = true;
    }
  }

  String _getCurrencies(int idCurrency) {
    String currency = '';
    if (currencies.isNotEmpty) {
      Currency object = currencies[idCurrency];
      if (SharedPrefController().getValueLang == 'en') {
        currency= object.nameEn;
      } else {
        currency= object.nameAr;
      }
    }
    return currency;
  }
}
