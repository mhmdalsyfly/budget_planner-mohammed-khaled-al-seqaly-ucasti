import 'package:budget_planner_app/model/category_actions.dart';
import 'package:budget_planner_app/provider_notify/actions_change_notifyer_controller.dart';
import 'package:budget_planner_app/utils/app_colors.dart';
import 'package:budget_planner_app/utils/size_config.dart';
import 'package:budget_planner_app/widgets/my_containerExpensesIncome.dart';
import 'package:budget_planner_app/widgets/my_contianer_logo.dart';
import 'package:budget_planner_app/widgets/my_text.dart';
import 'package:budget_planner_app/widgets/my_textfield1.dart';
import 'package:budget_planner_app/widgets/my_textfield2.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class ExpenseDetailsScreen extends StatefulWidget {
  late CategoryActions actions;

  ExpenseDetailsScreen({required this.actions});

  @override
  _ExpenseDetailsScreenState createState() => _ExpenseDetailsScreenState();
}

class _ExpenseDetailsScreenState extends State<ExpenseDetailsScreen> {
  late TextEditingController _categoryController;
  late TextEditingController _dateController;
  late TextEditingController _currencyController;
  late TextEditingController _noteController;

  Color _borderIncome = AppColors.WHITE;
  Color _borderExpenses = AppColors.WHITE;

  @override
  void initState() {
    _categoryController = TextEditingController(text: widget.actions.category);
    _dateController = TextEditingController(text: widget.actions.date);
    _currencyController = TextEditingController();
    _noteController = TextEditingController(text: widget.actions.notes);
    super.initState();
  }

  @override
  void didChangeDependencies() {
    _setTypeCurrency();
    _setBackgroundCurrency();
    super.didChangeDependencies();
  }

  @override
  void dispose() {
    _categoryController.dispose();
    _dateController.dispose();
    _currencyController.dispose();
    _noteController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.WHITE,
      appBar: AppBar(
        backgroundColor: AppColors.WHITE,
        title: MyText(
          title: AppLocalizations.of(context)!.expenseDetails,
        ),
        centerTitle: true,
        elevation: 0,
        actions: [
          Container(
            margin: EdgeInsetsDirectional.only(
              end: SizeConfig.scaleWidth(4),
            ),
            child: IconButton(
              icon: Icon(
                Icons.close,
                color: AppColors.GRAY,
              ),
              onPressed: () {
                Navigator.pop(context);
              },
            ),
          ),
        ],
      ),
      body: Padding(
        padding: EdgeInsets.symmetric(horizontal: SizeConfig.scaleWidth(20)),
        child: SingleChildScrollView(
          child: Column(
            children: [
              MyContianerLogo(
                child: Image.asset('images/icon_add_category.png'),
                height: SizeConfig.scaleHeight(112),
              ),
              SizedBox(height: SizeConfig.scaleHeight(13)),
              MyText(title: widget.actions.category),
              SizedBox(height: SizeConfig.scaleHeight(31)),
              MyText(
                title: widget.actions.amount.toString(),
                color: AppColors.Label_TextFiled,
                fontSize: 21,
                fontWeight: FontWeight.w600,
              ),
              SizedBox(height: SizeConfig.scaleHeight(33)),
              Row(
                children: [
                  MyContianerExpensesIncome(
                    title: AppLocalizations.of(context)!.income,
                    icon: 'images/arrow_down.png',
                    borderColor:
                        _borderIncome, //_getBackground(AppLocalizations.of(context)!.income);
                  ),
                  SizedBox(width: SizeConfig.scaleWidth(10)),
                  MyContianerExpensesIncome(
                    title: AppLocalizations.of(context)!.expenses,
                    icon: 'images/arrow_up.png',
                    borderColor: _borderExpenses,
                  ),
                ],
              ),
              SizedBox(height: SizeConfig.scaleHeight(11)),
              MyContianerLogo(
                borderRadius: 8,
                xOffset: 0,
                yOffset: 3,
                blur: 6,
                shadow: AppColors.BLACK.withOpacity(.16),
                paddingHorizontal: 17,
                paddingVertical: 0,
                width: double.infinity,
                child: Column(
                  children: [
                    MyTextField2(
                      title: AppLocalizations.of(context)!.category,
                      subTitle: AppLocalizations.of(context)!.none,
                      controller: _categoryController,
                      textInputType: TextInputType.number,
                      check: true,
                      enabled: false,
                    ),
                    MyTextField2(
                      title: AppLocalizations.of(context)!.date,
                      subTitle: AppLocalizations.of(context)!.none,
                      controller: _dateController,
                      textInputType: TextInputType.number,
                      check: true,
                      enabled: false,
                    ),
                    MyTextField2(
                      title: AppLocalizations.of(context)!.currency,
                      subTitle: AppLocalizations.of(context)!.dollar,
                      controller: _currencyController,
                      textInputType: TextInputType.number,
                      check: true,
                      enabled: false,
                    ),
                  ],
                ),
              ),
              SizedBox(height: SizeConfig.scaleHeight(11)),
              MyTextField1(
                controller: _noteController,
                textHint: AppLocalizations.of(context)!.addNote,
                blur: 6,
                radius: 8,
                height: SizeConfig.scaleHeight(112),
                alignment: AlignmentDirectional.topStart,
                shadow: AppColors.BLACK.withOpacity(.16),
                enabled: false,
              ),
            ],
          ),
        ),
      ),
    );
  }

  _getBackground(String title) {
    if (title == AppLocalizations.of(context)!.income) {
      _borderIncome = AppColors.GREEN;
      _borderExpenses = AppColors.WHITE;
    } else if (title == AppLocalizations.of(context)!.expenses) {
      _borderExpenses = AppColors.RED_TEXT_LISTVIEW;
      _borderIncome = AppColors.WHITE;
    }
    setState(() {});
  }

  void _setTypeCurrency() {
    switch (widget.actions.currencyId) {
      case 0:
        _currencyController.text = AppLocalizations.of(context)!.shakel;
        break;
      case 1:
        _currencyController.text = AppLocalizations.of(context)!.dDollar;
        break;
      case 2:
        _currencyController.text = AppLocalizations.of(context)!.jOD;
        break;
    }
  }

  void _setBackgroundCurrency() {
    if (widget.actions.expense) {
      _borderExpenses = AppColors.RED_TEXT_LISTVIEW;
      _borderIncome = AppColors.WHITE;
    } else {
      _borderIncome = AppColors.GREEN;
      _borderExpenses = AppColors.WHITE;
    }

    setState(() {});
  }
}
