import 'package:budget_planner_app/helpers/show_snakbar.dart';
import 'package:budget_planner_app/utils/app_colors.dart';
import 'package:budget_planner_app/utils/size_config.dart';
import 'package:budget_planner_app/widgets/my_contianer_logo.dart';
import 'package:budget_planner_app/widgets/my_text.dart';
import 'package:budget_planner_app/widgets/my_text_button.dart';
import 'package:budget_planner_app/widgets/textfield_pin_code.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class PinCodeScreen extends StatefulWidget {
  String? title;
  int? pinCodeOld;

  PinCodeScreen({this.title, this.pinCodeOld});

  @override
  _PinCodeScreenState createState() => _PinCodeScreenState();
}

class _PinCodeScreenState extends State<PinCodeScreen> {
  Color bg = AppColors.WHITE;
  String pinCode = '';

  String pin1 = '';
  String pin2 = '';
  String pin3 = '';
  String pin4 = '';

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Container(
          child: Scaffold(
            appBar: AppBar(
              backgroundColor: AppColors.WHITE,
              elevation: 0,
              iconTheme: IconThemeData(color: AppColors.BG_BUTTON),
            ),
            backgroundColor: AppColors.BG_SCREEN_COLOR,
            body: SingleChildScrollView(
              child: Column(
                children: [
                  SizedBox(
                    height: SizeConfig.scaleHeight(43),
                    width: double.infinity,
                  ),
                  MyText(title: getTitle()),
                  SizedBox(height: SizeConfig.scaleHeight(7)),
                  MyText(
                    title: AppLocalizations.of(context)!.newPIN,
                    color: AppColors.GRAY,
                    fontWeight: FontWeight.w400,
                    fontSize: 15,
                  ),
                  SizedBox(height: SizeConfig.scaleHeight(20)),
                  SizedBox(
                    height: SizeConfig.scaleHeight(55),
                    child: Row(
                      mainAxisSize: MainAxisSize.min,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        boxPinCode(pin1),
                        SizedBox(width: SizeConfig.scaleWidth(12)),
                        boxPinCode(pin2),
                        SizedBox(width: SizeConfig.scaleWidth(12)),
                        boxPinCode(pin3),
                        SizedBox(width: SizeConfig.scaleWidth(12)),
                        boxPinCode(pin4),
                      ],
                    ),
                  ),
                  SizedBox(height: SizeConfig.scaleHeight(50)),
                  allNumbers(),
                ],
              ),
            ),
          ),
        ),
        Positioned(
          bottom: SizeConfig.scaleHeight(80),
          right: SizeConfig.scaleWidth(20),
          left: SizeConfig.scaleWidth(20),
          child: MyTextButton(
            title: AppLocalizations.of(context)!.apply,
            checkBgColor: AppColors.BG_BUTTON,
            onPreesed: () async {
              if (pinCode.length == 4) {
                if (getTitle() == AppLocalizations.of(context)!.enterPasscode) {
                  Navigator.pop(context, pinCode);
                } else if (getTitle() ==
                    AppLocalizations.of(context)!.enterPasscodeOld) {
                  print(widget.pinCodeOld);
                  if (int.parse(pinCode) == widget.pinCodeOld) {
                     String newPin= await Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => PinCodeScreen(
                          title: AppLocalizations.of(context)!.enterPasscodeNew,
                        ),
                      ),
                    ) ?? '';
                     Navigator.pop(context,newPin);
                  } else {
                    Helpers.showSnackBar(context,
                        message: AppLocalizations.of(context)!
                            .passwordFieldDuplicate,
                        error: true);
                  }
                } else {
                  Navigator.pop(context, pinCode);
                }
              } else {
                Helpers.showSnackBar(
                  context,
                  message: AppLocalizations.of(context)!.messagePinCode,
                  error: true,
                );
              }
            },
          ),
        ),
      ],
    );
  }

  Color getColor(String text) {
    if (text.isNotEmpty) return AppColors.BG_BUTTON;
    return AppColors.WHITE;
  }

  Widget boxNum(int num, {bool check = true, bool delete = false}) {
    return GestureDetector(
      onTap: () {
        setState(() {
          if (!delete) {
            if (pinCode.length < 4) {
              pinCode += num.toString();
              fillPinCode();
            }
          } else {
            if (pinCode.length > 0)
              pinCode = pinCode.substring(0, pinCode.length - 1);
            deletePinCode();
          }
        });
      },
      child: MyContianerLogo(
        borderRadius: 19,
        xOffset: 0,
        yOffset: 10,
        blur: 18,
        shadow: AppColors.BLACK.withOpacity(.2),
        paddingHorizontal: 0,
        paddingVertical: 0,
        alignment: AlignmentDirectional.center,
        width: 70,
        height: SizeConfig.scaleHeight(72),
        child: check
            ? MyText(
                title: '$num',
                color: AppColors.BG_BUTTON,
                fontSize: 23,
              )
            : Icon(
                Icons.highlight_remove_sharp,
                color: AppColors.RED,
                size: 28,
              ),
      ),
    );
  }

  Column allNumbers() {
    return Column(
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            boxNum(1),
            marginWidth(),
            boxNum(2),
            marginWidth(),
            boxNum(3),
          ],
        ),
        marginHeight(),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            boxNum(4),
            marginWidth(),
            boxNum(5),
            marginWidth(),
            boxNum(6),
          ],
        ),
        marginHeight(),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            boxNum(7),
            marginWidth(),
            boxNum(8),
            marginWidth(),
            boxNum(9),
          ],
        ),
        marginHeight(),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            boxNum(100, check: false, delete: true),
            marginWidth(),
            boxNum(0),
            marginWidth(),
            emptyContainer(),
          ],
        ),
      ],
    );
  }

  SizedBox marginWidth() {
    return SizedBox(width: SizeConfig.scaleWidth(32));
  }

  Container boxPinCode(String text) {
    return Container(
      height: SizeConfig.scaleHeight(45),
      width: SizeConfig.scaleWidth(45),
      alignment: Alignment.center,
      decoration: BoxDecoration(
        color: getColor(text),
        borderRadius: BorderRadius.circular(8),
        boxShadow: [
          BoxShadow(
            color: AppColors.BLACK.withOpacity(.16),
            blurRadius: 6,
            offset: Offset(0, 0),
          ),
        ],
      ),
      child: MyText(
        title: text,
        color: AppColors.WHITE,
        fontSize: 23,
      ),
    );
  }

  Container emptyContainer() {
    return Container(
      width: SizeConfig.scaleWidth(70),
      height: SizeConfig.scaleHeight(72),
      color: Colors.transparent,
    );
  }

  SizedBox marginHeight() {
    return SizedBox(height: SizeConfig.scaleHeight(24));
  }

  void fillPinCode() {
    if (pinCode.length == 1) {
      pin1 = pinCode.substring(0, 1);
    } else if (pinCode.length == 2) {
      pin2 = pinCode.substring(1, 2);
    } else if (pinCode.length == 3) {
      pin3 = pinCode.substring(2, 3);
    } else if (pinCode.length == 4) {
      pin4 = pinCode.substring(3, 4);
    }
  }

  void deletePinCode() {
    if (pinCode.length == 0) {
      pin1 = '';
    } else if (pinCode.length == 1) {
      pin2 = '';
    } else if (pinCode.length == 2) {
      pin3 = '';
    } else if (pinCode.length == 3) {
      pin4 = '';
    }
  }

  String getTitle() {
    if (widget.title != null) {
      if (widget.title == AppLocalizations.of(context)!.enterPasscodeOld) {
        return AppLocalizations.of(context)!.enterPasscodeOld;
      } else if (widget.title ==
          AppLocalizations.of(context)!.enterPasscodeNew) {
        return AppLocalizations.of(context)!.enterPasscodeNew;
      }
    }
    return AppLocalizations.of(context)!.enterPasscode;
  }
}
