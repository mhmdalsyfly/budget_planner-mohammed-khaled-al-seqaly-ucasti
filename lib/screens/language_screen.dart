import 'package:budget_planner_app/provider_notify/language_notify_controller.dart';
import 'package:budget_planner_app/storage/pref/shared_pref_controller.dart';
import 'package:budget_planner_app/utils/app_colors.dart';
import 'package:budget_planner_app/utils/size_config.dart';
import 'package:budget_planner_app/widgets/container_settings.dart';
import 'package:budget_planner_app/widgets/my_text.dart';
import 'package:budget_planner_app/widgets/my_text_button.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:provider/provider.dart';

class LanguageScreen extends StatefulWidget {
  const LanguageScreen({Key? key}) : super(key: key);

  @override
  _LanguageScreenState createState() => _LanguageScreenState();
}

class _LanguageScreenState extends State<LanguageScreen> {
  bool check1 = true;
  bool check2 = false;

  @override
  void didChangeDependencies() {
    _checkLangauge(
      Provider.of<LanguageNotifyController>(context, listen: false)
          .locale
          .languageCode,
    );
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.WHITE,
      appBar: AppBar(
        centerTitle: true,
        elevation: 0,
        iconTheme: IconThemeData(color: AppColors.BG_BUTTON),
        backgroundColor: AppColors.WHITE,
        title: MyText(
          title: AppLocalizations.of(context)!.languages,
        ),
      ),
      body: Padding(
        padding: EdgeInsets.symmetric(horizontal: SizeConfig.scaleHeight(20)),
        child: Column(
          children: [
            SizedBox(height: SizeConfig.scaleHeight(35)),
            SizedBox(
              width: double.infinity,
              child: MyText(
                title: AppLocalizations.of(context)!.textLanguages,
                fontWeight: FontWeight.w500,
                fontSize: 15,
                textAlign: TextAlign.start,
              ),
            ),
            SizedBox(height: SizeConfig.scaleHeight(15)),
            ContainerSettings(
              title: AppLocalizations.of(context)!.ar,
              iconDataLeading: Icons.language,
              iconDataTralling: Icons.check,
              visible: check1,
              onPreased: () {
                _checkLangauge('ar');
              },
            ),
            SizedBox(height: SizeConfig.scaleHeight(15)),
            ContainerSettings(
              title: AppLocalizations.of(context)!.en,
              iconDataLeading: Icons.language,
              iconDataTralling: Icons.check,
              visible: check2,
              onPreased: () {
                _checkLangauge('en');
              },
            ),
            SizedBox(height: SizeConfig.scaleHeight(25)),
            MyTextButton(
              title: AppLocalizations.of(context)!.save,
              checkBgColor: AppColors.BG_BUTTON,
              onPreesed: () {
                setLang();
                setState(() {});
              },
            ),
          ],
        ),
      ),
    );
  }

  _checkLangauge(String lang) {
    if (lang == 'en') {
      check1 = false;
      check2 = true;
    } else {
      check1 = true;
      check2 = false;
    }
    setState(() {});
  }

  String _getLangauge() {
    if (check1) return 'ar';

    return 'en';
  }

  void setLang() {
    Provider.of<LanguageNotifyController>(context, listen: false)
        .changeLanguage(Locale(_getLangauge()));
    SharedPrefController().setValueLang(_getLangauge());
  }
}
