import 'package:budget_planner_app/helpers/show_snakbar.dart';
import 'package:budget_planner_app/model/user.dart';
import 'package:budget_planner_app/provider_notify/currency_change_notifyer_controller.dart';
import 'package:budget_planner_app/provider_notify/user_change_notifyer_controller.dart';
import 'package:budget_planner_app/storage/pref/shared_pref_controller.dart';
import 'package:budget_planner_app/utils/app_colors.dart';
import 'package:budget_planner_app/utils/size_config.dart';
import 'package:budget_planner_app/widgets/my_contianer_logo.dart';
import 'package:budget_planner_app/widgets/my_text.dart';
import 'package:budget_planner_app/widgets/my_text_button.dart';
import 'package:budget_planner_app/widgets/my_textfield1.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:provider/provider.dart';

class LoginScreen extends StatefulWidget {
  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  late TextEditingController _emailTextEditing;
  late TextEditingController _codeNumEditing;
  late TapGestureRecognizer _onTapGestureRecognizer;

  List<User> _listUser = [];


  @override
  void initState() {
    _emailTextEditing = TextEditingController();
    _codeNumEditing = TextEditingController();
    _onTapGestureRecognizer = TapGestureRecognizer();
    _onTapGestureRecognizer.onTap = navigationToRegister;
    super.initState();
  }

  @override
  void didChangeDependencies() {
    _listUser = Provider.of<UserChangeNotifierController>(context).listUsers;
    Provider.of<CurrencyChangeNotifierController>(context, listen: false)
        .readCurrency();
    super.didChangeDependencies();
  }

  void navigationToRegister() {
    // TODO:: GO TO NEXT SCREEN CREATE ACCOUNT
    Navigator.pushNamed(context, '/create_account_screen');
  }

  @override
  void dispose() {
    _emailTextEditing.dispose();
    _codeNumEditing.dispose();
    _onTapGestureRecognizer.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.BG_SCREEN_COLOR,
      body: Padding(
        padding: EdgeInsets.symmetric(horizontal: SizeConfig.scaleWidth(20)),
        child: SingleChildScrollView(
          child: Column(
            children: [
              SizedBox(height: SizeConfig.scaleHeight(81)),
              MyContianerLogo(
                child: Image.asset('images/icon_wallet.png'),
                height: SizeConfig.scaleHeight(112),
              ),
              SizedBox(height: SizeConfig.scaleHeight(13)),
              MyText(title: AppLocalizations.of(context)!.login),
              SizedBox(height: SizeConfig.scaleHeight(11)),
              MyText(
                title: AppLocalizations.of(context)!.enterEmailPinCode,
                fontSize: 15,
                fontWeight: FontWeight.w400,
                color: AppColors.GRAY,
              ),
              SizedBox(height: SizeConfig.scaleHeight(50)),
              MyTextField1(
                controller: _emailTextEditing,
                textInputType: TextInputType.emailAddress,
                textHint: AppLocalizations.of(context)!.email,
              ),
              SizedBox(height: SizeConfig.scaleHeight(15)),
              MyTextField1(
                controller: _codeNumEditing,
                textHint: AppLocalizations.of(context)!.pinCode,
              ),
              SizedBox(height: SizeConfig.scaleHeight(30)),
              MyTextButton(
                title: AppLocalizations.of(context)!.login,
                onPreesed: () {
                  preformLogin();
                },
              ),
              SizedBox(height: SizeConfig.scaleHeight(20)),
              RichText(
                text: TextSpan(
                  text: AppLocalizations.of(context)!.dontHaveAccount,
                  style: TextStyle(
                    fontSize: 15,
                    fontWeight: FontWeight.w400,
                    fontFamily: 'Montserrat',
                    color: AppColors.GRAY,
                  ),
                  children: [
                    TextSpan(
                      recognizer: _onTapGestureRecognizer,
                      text: AppLocalizations.of(context)!.createNow,
                      style: TextStyle(
                        color: AppColors.RICH_TEXT,
                        fontSize: 15,
                        fontWeight: FontWeight.w400,
                        fontFamily: 'Montserrat',
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  preformLogin() {
    bool checkLogin = _checkData();
    if (checkLogin) {
      _checkLogin();
    } else {
      Helpers.showSnackBar(
        context,
        message: AppLocalizations.of(context)!.messagePinCode,
        error: true,
      );
    }
  }

  bool _checkData() {
    if (_emailTextEditing.text.trim().isNotEmpty &&
        _codeNumEditing.text.trim().isNotEmpty) {
      return true;
    }
    return false;
  }

  void _checkLogin() async {
    String email = _emailTextEditing.text.trim();
    int pinCode = int.parse(_codeNumEditing.text.trim());
    int successLogin = _processLogin(email, pinCode);

    Helpers.showSnackBar(
      context,
      message: successLogin >= 0
          ? AppLocalizations.of(context)!.messageLoginSuccess
          : AppLocalizations.of(context)!.messageLoginField,
      error: !(successLogin >= 0),
    );

    if (successLogin >= 0) {
      User user =_listUser[successLogin];
      await SharedPrefController().setUser(user);
      Provider.of<UserChangeNotifierController>(context,listen: false).setUser(user);
      _navigatorMainBottomNavigatorScreen();
    }
  }

  int _processLogin(String email, int _pinCode) {
    for (int i = 0; i < _listUser.length; i++) {
      if (_listUser[i].email.toLowerCase() == email &&
          _listUser[i].pin == _pinCode) {
        return i;
      }
    }
    return -1;
  }

  _navigatorMainBottomNavigatorScreen() {
    Navigator.pushNamedAndRemoveUntil(
      context,
      '/main_bottom_navigation_screen',
      (route) => false,
    );
  }
}
