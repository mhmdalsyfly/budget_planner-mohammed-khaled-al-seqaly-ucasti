import 'package:budget_planner_app/helpers/show_snakbar.dart';
import 'package:budget_planner_app/model/user.dart';
import 'package:budget_planner_app/provider_notify/actions_change_notifyer_controller.dart';
import 'package:budget_planner_app/provider_notify/category_change_notifyer_controller.dart';
import 'package:budget_planner_app/provider_notify/user_change_notifyer_controller.dart';
import 'package:budget_planner_app/storage/pref/shared_pref_controller.dart';
import 'package:budget_planner_app/utils/app_colors.dart';
import 'package:budget_planner_app/utils/size_config.dart';
import 'package:budget_planner_app/widgets/container_settings.dart';
import 'package:budget_planner_app/widgets/my_text.dart';
import 'package:budget_planner_app/widgets/my_text_button2.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:provider/provider.dart';

class SettingsScreen extends StatefulWidget {
  @override
  _SettingsScreenState createState() => _SettingsScreenState();
}

class _SettingsScreenState extends State<SettingsScreen> {
  @override
  void didChangeDependencies() {
    SizeConfig().init(context);
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
      statusBarColor: AppColors.WHITE,
      statusBarBrightness: Brightness.dark,
    ));
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.WHITE,
      appBar: AppBar(
        backgroundColor: AppColors.WHITE,
        title: MyText(title: AppLocalizations.of(context)!.settings),
        centerTitle: true,
        elevation: 0,
        iconTheme: IconThemeData(color: AppColors.BG_BUTTON),
      ),
      body: Padding(
        padding: EdgeInsets.symmetric(horizontal: SizeConfig.scaleHeight(20)),
        child: Column(
          children: [
            SizedBox(height: SizeConfig.scaleHeight(35)),
            SizedBox(
              width: double.infinity,
              child: MyText(
                title: AppLocalizations.of(context)!.general,
                fontWeight: FontWeight.w500,
                fontSize: 15,
                textAlign: TextAlign.start,
              ),
            ),
            SizedBox(height: SizeConfig.scaleHeight(15)),
            ContainerSettings(
              title: AppLocalizations.of(context)!.aboutApp,
              iconDataLeading: Icons.info,
              iconDataTralling: Icons.arrow_forward_ios,
              onPreased: () {
                _navigatorToInfoAppScreen();
              }, //
            ),
            SizedBox(height: SizeConfig.scaleHeight(15)),
            ContainerSettings(
              title: AppLocalizations.of(context)!.language,
              iconDataLeading: Icons.language,
              iconDataTralling: Icons.arrow_forward_ios,
              onPreased: () {
                Navigator.pushNamed(context, '/language_screen');
              },
            ),
            SizedBox(height: SizeConfig.scaleHeight(15)),
            ContainerSettings(
              title: AppLocalizations.of(context)!.logout,
              iconDataLeading: Icons.login,
              visible: false,
              onPreased: () {
                _logoutAccount();
              },
            ),
            SizedBox(height: SizeConfig.scaleHeight(25)),
            SizedBox(
              width: double.infinity,
              child: MyText(
                title: AppLocalizations.of(context)!.accountAndData,
                fontSize: 15,
                textAlign: TextAlign.start,
                fontWeight: FontWeight.w500,
              ),
            ),
            SizedBox(height: SizeConfig.scaleHeight(15)),
            ContainerSettings(
              title: AppLocalizations.of(context)!.clearAccountData,
              iconDataLeading: Icons.delete_forever,
              textColor: AppColors.RED_Button_DELETE,
              border: false,
              iconColor: AppColors.RED_Button_DELETE,
              visible: false,
              onPreased: () {
                cleardDataUser();
              },
            ),
            SizedBox(height: SizeConfig.scaleHeight(15)),
            ContainerSettings(
              title: AppLocalizations.of(context)!.removeAccount,
              iconDataLeading: Icons.person_remove,
              border: false,
              textColor: AppColors.WHITE,
              iconColor: AppColors.WHITE,
              bgColor: AppColors.RED_Button_DELETE,
              visible: false,
              onPreased: () {
                showDialog(
                  context: context,
                  builder: (context) => showAlertDialog(),
                  barrierDismissible: false,
                );
              },
            ),
          ],
        ),
      ),
    );
  }

  AlertDialog showAlertDialog() {
    return AlertDialog(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(10),
        side: BorderSide(
          color: AppColors.RED_Button_DELETE,
        ),
      ),
      title: MyText(
        title: AppLocalizations.of(context)!.clearAccountData,
        fontSize: 15,
        textAlign: TextAlign.start,
        fontWeight: FontWeight.w500,
      ),
      content: MyText(
        title: AppLocalizations.of(context)!.warningMessage,
        textAlign: TextAlign.start,
        fontSize: 12,
        fontWeight: FontWeight.w400,
      ),
      actions: [
        MyTextButton2(
          context: context,
          title: AppLocalizations.of(context)!.no,
          onPreese: () {
            Navigator.pop(context);
          },
        ),
        MyTextButton2(
          context: context,
          title: AppLocalizations.of(context)!.yes,
          colorYes: true,
          onPreese: () {
            Navigator.pop(context);
            _deleteAccount();
          },
        ),
      ],
    );
  }

  void _navigatorToInfoAppScreen() {
    Navigator.pushNamed(context, '/info_app_screen');
  }

  _logoutAccount() {
    User emptyUser = User(
      id: -1,
      name: '',
      email: '',
      currencyId: 0,
      dayLimit: -1,
      pin: -1,
    );
    SharedPrefController().setUser(emptyUser);

    Provider.of<UserChangeNotifierController>(context, listen: false)
        .setUser(emptyUser);

    clearDataProviders();
    Navigator.pushNamedAndRemoveUntil(
      context,
      '/login_screen',
      (route) => false,
    );
  }

  Future<void> cleardDataUser() async {
    await Provider.of<UserChangeNotifierController>(context, listen: false)
        .clearActionUser();

    clearDataProviders();

    Helpers.showSnackBar(
      context,
      message: AppLocalizations.of(context)!.successClearData,
    );
  }

  void _deleteAccount() {
    cleardDataUser();
    Provider.of<UserChangeNotifierController>(context, listen: false)
        .delete(SharedPrefController().getUser().id!);
    Helpers.showSnackBar(context,
        message: AppLocalizations.of(context)!.deleteAccount);
    Navigator.pushNamedAndRemoveUntil(
        context, '/login_screen', (route) => false);
  }

  void clearDataProviders() {
    Provider.of<ActionsChangeNotifierController>(context, listen: false)
        .clearDataAction();
    Provider.of<CategoryChangeNotifierController>(context, listen: false)
        .clearDataUser();
  }
}
