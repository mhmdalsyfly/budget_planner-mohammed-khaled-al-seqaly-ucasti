import 'package:budget_planner_app/tab_bar/expenses_income2.dart';
import 'package:budget_planner_app/utils/app_colors.dart';
import 'package:budget_planner_app/utils/size_config.dart';
import 'package:budget_planner_app/widgets/my_contianer_logo.dart';
import 'package:budget_planner_app/widgets/my_text.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class ChooseCategoryScreen extends StatefulWidget {
  const ChooseCategoryScreen({Key? key}) : super(key: key);

  @override
  _ChooseCategoryScreenState createState() => _ChooseCategoryScreenState();
}

class _ChooseCategoryScreenState extends State<ChooseCategoryScreen> {
  int _currentPage = 0;
  Color _textColor1 = AppColors.WHITE;
  Color _bgColor1 = AppColors.BG_BUTTON;
  Color _textColor2 = AppColors.Label_TextFiled;
  Color _bgColor2 = AppColors.BG_PROGRESS;
  List<ExpensesIncome2> _list = [];

  @override
  void didChangeDependencies() {
    _list = [
      ExpensesIncome2(title: AppLocalizations.of(context)!.income),
      ExpensesIncome2(title: AppLocalizations.of(context)!.expenses),
    ];
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.WHITE,
      appBar: AppBar(
        title: MyText(title: AppLocalizations.of(context)!.categories),
        elevation: 0,
        centerTitle: true,
        backgroundColor: AppColors.WHITE,
        iconTheme: IconThemeData(color: AppColors.BG_BUTTON),
      ),
      body: Padding(
        padding: EdgeInsets.symmetric(horizontal: SizeConfig.scaleWidth(20)),
        child: Column(
          children: [
            SizedBox(height: SizeConfig.scaleHeight(20)),
            myCusttomTapBar(),
            SizedBox(height: SizeConfig.scaleHeight(21)),
            Expanded(
              child: Container(
                height: double.infinity,
                width: double.infinity,
                child: _list[_currentPage],
              ),
            ),
          ],
        ),
      ),
    );
  }

  MyContianerLogo myCusttomTapBar() {
    return MyContianerLogo(
      height: SizeConfig.scaleHeight(38),
      width: double.infinity,
      enableShadow: false,
      bg_Container: AppColors.BG_PROGRESS,
      paddingVertical: 0,
      paddingHorizontal: 0,
      borderRadius: 20,
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          myItemTabBar(
            title: AppLocalizations.of(context)!.income,
            bgColor: _bgColor1,
            textColor: _textColor1,
          ),
          myItemTabBar(
            title: AppLocalizations.of(context)!.expenses,
            bgColor: _bgColor2,
            textColor: _textColor2,
          ),
        ],
      ),
    );
  }

  Expanded myItemTabBar({
    required String title,
    Color textColor = AppColors.Label_TextFiled,
    Color bgColor = AppColors.BG_PROGRESS,
  }) {
    getColorItemTapBar();
    return Expanded(
      child: Container(
        clipBehavior: Clip.antiAlias,
        height: double.infinity,
        alignment: Alignment.center,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(20),
        ),
        child: Material(
          color: bgColor,
          child: InkWell(
            onTap: () {
              choseScreen(title);
              getColorItemTapBar();
              setState(() {});
            },
            child: Center(
              child: MyText(
                title: title,
                fontSize: 13,
                color: textColor,
              ),
            ),
          ),
        ),
      ),
    );
  }

  void choseScreen(String title) {
    if (title == AppLocalizations.of(context)!.income) {
      _currentPage = 0;
    } else if (title == AppLocalizations.of(context)!.expenses) {
      _currentPage = 1;
    }
    setState(() {
      _list[_currentPage];
    });
  }

  void getColorItemTapBar() {
    switch (_currentPage) {
      case 0:
        _bgColor1 = AppColors.BG_BUTTON;
        _textColor1 = AppColors.WHITE;
        _bgColor2 = AppColors.BG_PROGRESS;
        _textColor2 = AppColors.Label_TextFiled;
        break;
      case 1:
        _bgColor1 = AppColors.BG_PROGRESS;
        _textColor1 = AppColors.Label_TextFiled;
        _bgColor2 = AppColors.BG_BUTTON;
        _textColor2 = AppColors.WHITE;
        break;
    }
  }
}
