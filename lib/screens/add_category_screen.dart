import 'package:budget_planner_app/helpers/show_snakbar.dart';
import 'package:budget_planner_app/model/category.dart';
import 'package:budget_planner_app/provider_notify/category_change_notifyer_controller.dart';
import 'package:budget_planner_app/storage/pref/shared_pref_controller.dart';
import 'package:budget_planner_app/utils/app_colors.dart';
import 'package:budget_planner_app/utils/size_config.dart';
import 'package:budget_planner_app/widgets/my_containerExpensesIncome.dart';
import 'package:budget_planner_app/widgets/my_contianer_logo.dart';
import 'package:budget_planner_app/widgets/my_text.dart';
import 'package:budget_planner_app/widgets/my_text_button.dart';
import 'package:budget_planner_app/widgets/my_textfield1.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:provider/provider.dart';

class AddCategoryScreen extends StatefulWidget {
  @override
  _AddCategoryScreenState createState() => _AddCategoryScreenState();
}

class _AddCategoryScreenState extends State<AddCategoryScreen> {
  late TextEditingController _enterCategoryController;

  Color _borderIncome = AppColors.WHITE;
  Color _borderExpenses = AppColors.WHITE;
  Color _colorButtonAdd = AppColors.BG_BUTTON_GRAY;
  int? _expense;

  @override
  void initState() {
    _enterCategoryController = TextEditingController();
    super.initState();
  }

  @override
  void dispose() {
    _enterCategoryController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.WHITE,
      appBar: AppBar(
        backgroundColor: AppColors.WHITE,
        elevation: 0,
        automaticallyImplyLeading: false,
        actions: [
          Container(
            margin: EdgeInsetsDirectional.only(end: SizeConfig.scaleWidth(4)),
            child: IconButton(
              icon: Icon(
                Icons.close,
                color: AppColors.GRAY,
              ),
              onPressed: () {
                Navigator.pop(context);
              },
            ),
          ),
        ],
      ),
      body: Padding(
        padding: EdgeInsets.symmetric(horizontal: SizeConfig.scaleWidth(20)),
        child: Column(
          children: [
            MyContianerLogo(
              child: Image.asset('images/icon_add_category.png'),
              height: SizeConfig.scaleHeight(112),
            ),
            SizedBox(height: SizeConfig.scaleHeight(13)),
            MyText(title: AppLocalizations.of(context)!.addCategory),
            SizedBox(
                height: SizeConfig.scaleHeight(SizeConfig.scaleHeight(50))),
            Row(
              children: [
                MyContianerExpensesIncome(
                  title: AppLocalizations.of(context)!.income,
                  icon: 'images/arrow_down.png',
                  borderColor: _borderIncome,
                  onPressed: () {
                    setState(() {
                      _getBackground(AppLocalizations.of(context)!.income);
                    });
                  },
                ),
                SizedBox(width: SizeConfig.scaleWidth(10)),
                MyContianerExpensesIncome(
                  title: AppLocalizations.of(context)!.expenses,
                  icon: 'images/arrow_up.png',
                  borderColor: _borderExpenses,
                  onPressed: () {
                    setState(() {
                      _getBackground(AppLocalizations.of(context)!.expenses);
                    });
                  },
                ),
              ],
            ),
            SizedBox(height: SizeConfig.scaleHeight(11)),
            MyTextField1(
              controller: _enterCategoryController,
              textHint: AppLocalizations.of(context)!.enterCategory,
              blur: 14,
              height: SizeConfig.scaleHeight(67),
              hOffset: 3,
              onChange: (String value) {
                if (_checkData()) {
                  _colorButtonAdd = AppColors.BG_BUTTON;
                } else {
                  _colorButtonAdd = AppColors.BG_BUTTON_GRAY;
                }
                setState(() {});
              },
            ),
            SizedBox(
              height: SizeConfig.scaleHeight(30),
            ),
            MyTextButton(
              title: AppLocalizations.of(context)!.add,
              checkBgColor: _colorButtonAdd,
              onPreesed: () {
                _preformAddCategory();
              },
            ),
          ],
        ),
      ),
    );
  }

  _getBackground(String title) {
    if (title == AppLocalizations.of(context)!.income) {
      _borderIncome = AppColors.GREEN;
      _borderExpenses = AppColors.WHITE;
      _expense = 0;
    } else if (title == AppLocalizations.of(context)!.expenses) {
      _borderExpenses = AppColors.RED_TEXT_LISTVIEW;
      _borderIncome = AppColors.WHITE;
      _expense = 1;
    }
    if (_checkData()) {
      _colorButtonAdd = AppColors.BG_BUTTON;
    } else {
      _colorButtonAdd = AppColors.BG_BUTTON_GRAY;
    }
    setState(() {});
  }

  _preformAddCategory() {
    if (_checkData()) {
      _saveDataCategory();
    } else {
      Helpers.showSnackBar(
        context,
        message: AppLocalizations.of(context)!.messagePinCode,
        error: true,
      );
    }
  }

  bool _checkData() {
    if ((_borderIncome == AppColors.WHITE &&
            _borderExpenses == AppColors.WHITE) ||
        _enterCategoryController.text.trim().isEmpty) {
      return false;
    }
    return true;
  }

  void _saveDataCategory() async {
    int createdCategory = await Provider.of<CategoryChangeNotifierController>(
            context,
            listen: false)
        .create(category);

    bool created = createdCategory > 0;
    Helpers.showSnackBar(
      context,
      message: created
          ? AppLocalizations.of(context)!.messageCreateCategory
          : AppLocalizations.of(context)!.messageNotCreateCategory,
      error: !created ? true : false,
    );
    if (created) {
      Navigator.pop(context);
    }
  }

  Category get category {
    return Category(
      name: _enterCategoryController.text.trim(),
      expense: _expense!,
      userId: SharedPrefController().getUser().id!,
    );
  }
}
