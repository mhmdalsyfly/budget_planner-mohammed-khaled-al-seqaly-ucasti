import 'package:budget_planner_app/storage/pref/shared_pref_controller.dart';
import 'package:budget_planner_app/utils/app_colors.dart';
import 'package:budget_planner_app/utils/size_config.dart';
import 'package:budget_planner_app/widgets/my_contianer_logo.dart';
import 'package:budget_planner_app/widgets/my_text.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class InfoAppScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: AppColors.WHITE,
        elevation: 0,
        title: MyText(
          title: AppLocalizations.of(context)!.about,
        ),
        centerTitle: true,
        iconTheme: IconThemeData(color: AppColors.BG_BUTTON),
      ),
      backgroundColor: AppColors.BG_SCREEN_COLOR,
      body: Center(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            Spacer(),
            MyContianerLogo(
              width: 170,
              height: SizeConfig.scaleHeight(170),
              child: Image.asset(
                'images/logo.png',
              ),
              borderRadius: 45,
              paddingHorizontal: 42,
              paddingVertical: 24,
            ),
            SizedBox(height: SizeConfig.scaleHeight(29)),
            MyText(
              title: AppLocalizations.of(context)!.nameApp,
              fontSize: 24,
              fontWeight: FontWeight.w800,
            ),
            SizedBox(height: SizeConfig.scaleHeight(15)),
            MyText(
              title: 'PalLancer - Flutter',
              fontWeight: FontWeight.w300,
              fontSize: 18,
            ),
            SizedBox(height: SizeConfig.scaleHeight(8)),
            MyText(
              title: SharedPrefController().getUser().name,
              fontWeight: FontWeight.w300,
              fontSize: 15,
            ),
            Spacer(),
            MyText(
              title: 'v 1.0.0',
              fontSize: 15,
              fontWeight: FontWeight.w300,
            ),
            SizedBox(height: SizeConfig.scaleHeight(20)),
          ],
        ),
      ),
    );
  }
}
