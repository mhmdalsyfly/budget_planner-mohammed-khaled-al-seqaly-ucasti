import 'package:budget_planner_app/utils/app_colors.dart';
import 'package:budget_planner_app/utils/size_config.dart';
import 'package:budget_planner_app/widgets/my_contianer_logo.dart';
import 'package:budget_planner_app/widgets/my_text.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class CreatedAccountScreen extends StatefulWidget {
  @override
  _CreatedAccountScreenState createState() => _CreatedAccountScreenState();
}

class _CreatedAccountScreenState extends State<CreatedAccountScreen> {
  @override
  void initState() {
    Future.delayed(Duration(seconds: 3), () {
      Navigator.pushReplacementNamed(context, '/main_bottom_navigation_screen');
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.BG_SCREEN_COLOR,
      body: Center(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            MyContianerLogo(
              child: Image.asset('images/ic_like.png'),
              height: SizeConfig.scaleHeight(112),
            ),
            SizedBox(height: SizeConfig.scaleHeight(29)),
            MyText(
                title: AppLocalizations.of(context)!.congratulations,
                fontSize: 15),
            SizedBox(height: SizeConfig.scaleHeight(12)),
            MyText(
              title: AppLocalizations.of(context)!.haveRegistered,
              fontSize: 15,
              fontWeight: FontWeight.w400,
              color: AppColors.GRAY,
            ),
          ],
        ),
      ),
    );
  }
}
