import 'package:budget_planner_app/helpers/mixin.dart';
import 'package:budget_planner_app/helpers/show_snakbar.dart';
import 'package:budget_planner_app/model/category_actions.dart';
import 'package:budget_planner_app/model/currency.dart';
import 'package:budget_planner_app/model/name_type_category.dart';
import 'package:budget_planner_app/model/user.dart';
import 'package:budget_planner_app/provider_notify/actions_change_notifyer_controller.dart';
import 'package:budget_planner_app/provider_notify/category_change_notifyer_controller.dart';
import 'package:budget_planner_app/provider_notify/currency_change_notifyer_controller.dart';
import 'package:budget_planner_app/provider_notify/user_change_notifyer_controller.dart';
import 'package:budget_planner_app/storage/pref/shared_pref_controller.dart';
import 'package:budget_planner_app/utils/app_colors.dart';
import 'package:budget_planner_app/utils/size_config.dart';
import 'package:budget_planner_app/widgets/my_containerExpensesIncome.dart';
import 'package:budget_planner_app/widgets/my_contianer_logo.dart';
import 'package:budget_planner_app/widgets/my_text.dart';
import 'package:budget_planner_app/widgets/my_text_button.dart';
import 'package:budget_planner_app/widgets/my_textfield1.dart';
import 'package:budget_planner_app/widgets/my_textfield2.dart';
import 'package:budget_planner_app/widgets/my_textfield_currency.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:provider/provider.dart';

class AddOperationScreen extends StatefulWidget {
  @override
  _AddOperationScreenState createState() => _AddOperationScreenState();
}

class _AddOperationScreenState extends State<AddOperationScreen> with Transformation {
  DateTime selectedDate = DateTime.now();

  Future<void> _selectDate(BuildContext context) async {
    final DateTime? picked = await showDatePicker(
        context: context,
        initialDate: selectedDate,
        firstDate: DateTime(2015, 8),
        lastDate: DateTime(2101));
    if (picked != null && picked != selectedDate) {
      setState(() {
        selectedDate = picked;
      });
      _dateController.text = "${selectedDate.toLocal()}".split(' ')[0];
    }
  }

  late TextEditingController _categoryController;
  late TextEditingController _dateController;
  late TextEditingController _currencyController;
  late TextEditingController _typeCurrencyController;
  late TextEditingController _noteController;

  Color _borderIncome = AppColors.WHITE;
  Color _borderExpenses = AppColors.WHITE;
  Color _bgButtonCreate = AppColors.BG_BUTTON_GRAY;

  int? _currencyId;
  bool _expense = false;
  NameTypeCategory? _nameTypeCategory;

  @override
  void initState() {
    _currencyController = TextEditingController();
    _categoryController = TextEditingController();
    _typeCurrencyController = TextEditingController();
    _dateController = TextEditingController();
    _noteController = TextEditingController();
    super.initState();
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
  }

  @override
  void dispose() {
    _currencyController.dispose();
    _categoryController.dispose();
    _dateController.dispose();
    _noteController.dispose();
    _typeCurrencyController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.WHITE,
      appBar: AppBar(
        backgroundColor: AppColors.WHITE,
        elevation: 0,
        actions: [
          Container(
            margin: EdgeInsetsDirectional.only(
              end: SizeConfig.scaleWidth(4),
            ),
            child: IconButton(
              icon: Icon(
                Icons.close,
                color: AppColors.GRAY,
              ),
              onPressed: () {
                Navigator.pop(context);
              },
            ),
          ),
        ],
        automaticallyImplyLeading: false,
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: EdgeInsets.symmetric(horizontal: SizeConfig.scaleWidth(20)),
          child: Column(
            children: [
              MyContianerLogo(
                child: Image.asset('images/icon_add_category.png'),
                height: SizeConfig.scaleHeight(112),
              ),
              SizedBox(height: SizeConfig.scaleHeight(13)),
              MyText(title: AppLocalizations.of(context)!.addOperation),
              SizedBox(height: SizeConfig.scaleHeight(24)),
              MyTextFieldCurrency(
                controller: _currencyController,
                onChange: (String value) {
                  if (_checkData()) {
                    _bgButtonCreate = AppColors.BG_BUTTON;
                  } else {
                    _bgButtonCreate = AppColors.BG_BUTTON_GRAY;
                  }
                  setState(() {});
                },
              ),
              SizedBox(height: SizeConfig.scaleHeight(24)),
              Row(
                children: [
                  MyContianerExpensesIncome(
                    title: AppLocalizations.of(context)!.income,
                    icon: 'images/arrow_down.png',
                    borderColor: _borderIncome,
                  ),
                  SizedBox(width: SizeConfig.scaleWidth(10)),
                  MyContianerExpensesIncome(
                    title: AppLocalizations.of(context)!.expenses,
                    icon: 'images/arrow_up.png',
                    borderColor: _borderExpenses,
                  ),
                ],
              ),
              SizedBox(height: SizeConfig.scaleHeight(11)),
              MyContianerLogo(
                borderRadius: 8,
                xOffset: 0,
                yOffset: 3,
                blur: 6,
                shadow: AppColors.BLACK.withOpacity(.16),
                paddingHorizontal: 17,
                paddingVertical: 0,
                width: double.infinity,
                child: Column(
                  children: [
                    MyTextField2(
                      title: AppLocalizations.of(context)!.category,
                      subTitle: AppLocalizations.of(context)!.none,
                      controller: _categoryController,
                      textInputType: TextInputType.number,
                      check: true,
                      enabled: false,
                      onTap: () {
                        _getDataFromBackCategoryScreen();
                      },
                      onChange: (String value) {
                        if (_checkData()) {
                          _bgButtonCreate = AppColors.BG_BUTTON;
                        } else {
                          _bgButtonCreate = AppColors.BG_BUTTON_GRAY;
                        }
                        setState(() {});
                      },
                    ),
                    MyTextField2(
                      title: AppLocalizations.of(context)!.date,
                      subTitle: AppLocalizations.of(context)!.none,
                      controller: _dateController,
                      textInputType: TextInputType.number,
                      check: true,
                      enabled: false,
                      onTap: () {
                        _selectDate(context);
                      },
                    ),
                    MyTextField2(
                      title: AppLocalizations.of(context)!.currency,
                      subTitle: AppLocalizations.of(context)!.none,
                      controller: _typeCurrencyController,
                      textInputType: TextInputType.number,
                      check: true,
                      enabled: false,
                      onTap: () {
                        _getDataFromBackCurrencyScreen();
                      },
                    ),
                  ],
                ),
              ),
              SizedBox(height: SizeConfig.scaleHeight(11)),
              MyTextField1(
                controller: _noteController,
                textHint: AppLocalizations.of(context)!.addNote,
                height: SizeConfig.scaleHeight(112),
                alignment: AlignmentDirectional.topStart,
              ),
              SizedBox(height: SizeConfig.scaleHeight(24)),
              MyTextButton(
                title: AppLocalizations.of(context)!.add,
                checkBgColor: _bgButtonCreate,
                onPreesed: () {
                  _preformData();
                },
              )
            ],
          ),
        ),
      ),
    );
  }

  _getBackground(String? title) {
    if (title != null) if (title == AppLocalizations.of(context)!.income) {
      _borderIncome = AppColors.GREEN;
      _borderExpenses = AppColors.WHITE;
      _expense = false;
    } else if (title == AppLocalizations.of(context)!.expenses) {
      _borderExpenses = AppColors.RED_TEXT_LISTVIEW;
      _borderIncome = AppColors.WHITE;
      _expense = true;
    }
    setState(() {});
  }

  void _getDataFromBackCategoryScreen() async {
    _nameTypeCategory =
        await Navigator.pushNamed(context, '/chose_category_screen')
            as NameTypeCategory;

    _getBackground(_nameTypeCategory?.type);
    _setDataInTextField(_nameTypeCategory?.name);

    if (_checkData()) {
      _bgButtonCreate = AppColors.BG_BUTTON;
    } else {
      _bgButtonCreate = AppColors.BG_BUTTON_GRAY;
    }
    setState(() {});
  }

  bool _checkData() {
    if (_currencyController.text.isNotEmpty &&
        int.parse(_currencyController.text.toString()) > 0 &&
        _categoryController.text.isNotEmpty &&
        _dateController.text.isNotEmpty &&
        _typeCurrencyController.text.isNotEmpty &&
        (_borderExpenses != AppColors.WHITE ||
            _borderIncome != AppColors.WHITE)) {
      return true;
    }
    return false;
  }

  void _getDataFromBackCurrencyScreen() async {
    _currencyId =
        await Navigator.pushNamed(context, '/chose_currency_screen') as int;

    _choseCurrency(_currencyId);

    if (_checkData()) {
      _bgButtonCreate = AppColors.BG_BUTTON;
    } else {
      _bgButtonCreate = AppColors.BG_BUTTON_GRAY;
    }
    setState(() {});
  }

  _choseCurrency(int? current) {
    var currencies =
        Provider.of<CurrencyChangeNotifierController>(context, listen: false)
            .listCurrency;

    if (currencies.isNotEmpty) {
      Currency object = currencies[current!];
      if (SharedPrefController().getValueLang == 'en') {
        _typeCurrencyController.text = object.nameEn;
      } else {
        _typeCurrencyController.text = object.nameAr;
      }
    }
  }

  void _setDataInTextField(String? name) {
    if (name != null) _categoryController.text = name;
  }

  CategoryActions get action {
    return CategoryActions(
      amount: int.parse(_currencyController.text),
      date: _dateController.text,
      expense: _expense,
      notes: _noteController.text,
      category: _nameTypeCategory!.name,
      userId: SharedPrefController().getUser().id!,
      categoryId: _nameTypeCategory!.id,
      currencyId: _currencyId!,
    );
  }

  void _preformData() {
    if (_checkData()) {
      _saveDataAction();
    } else {
      Helpers.showSnackBar(
        context,
        message: AppLocalizations.of(context)!.messagePinCode,
        error: true,
      );
    }
  }

  void _saveDataAction() async {
    if (1 -
            ((Provider.of<ActionsChangeNotifierController>(context,
                            listen: false)
                        .wallet2 +
                    currencyTransformation(
                        amount: action.amount.toDouble(),
                        idCurrency: action.currencyId)) /
                SharedPrefController().getUser().dayLimit) <=
        0) {
      Helpers.showSnackBar(
        context,
        message: AppLocalizations.of(context)!.maxExceeded,
        error: true,
      );
      return;
    }

    int addAction = await Provider.of<ActionsChangeNotifierController>(context,
            listen: false)
        .create(action);

    await Provider.of<CategoryChangeNotifierController>(context, listen: false)
        .readCategoryExpenses();

    await Provider.of<CategoryChangeNotifierController>(context, listen: false)
        .readCategoryIncome();

    Provider.of<ActionsChangeNotifierController>(context, listen: false)
        .setAmount(
      idCurrency: action.currencyId,
      amount: action.amount.toDouble(),
      expense: action.expense,
    );

    bool created = addAction > 0;

    Helpers.showSnackBar(
      context,
      message: created
          ? AppLocalizations.of(context)!.messageAddAction
          : AppLocalizations.of(context)!.messageProblemAddAction,
      error: !created ? true : false,
    );

    if (created) {
      Navigator.pushReplacementNamed(context, '/new_operation_success');
    }
  }
}
