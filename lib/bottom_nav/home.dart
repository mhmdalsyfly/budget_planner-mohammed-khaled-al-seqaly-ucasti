import 'package:budget_planner_app/model/category_actions.dart';
import 'package:budget_planner_app/model/currency.dart';
import 'package:budget_planner_app/provider_notify/actions_change_notifyer_controller.dart';
import 'package:budget_planner_app/provider_notify/currency_change_notifyer_controller.dart';
import 'package:budget_planner_app/provider_notify/user_change_notifyer_controller.dart';
import 'package:budget_planner_app/storage/pref/shared_pref_controller.dart';
import 'package:budget_planner_app/utils/app_colors.dart';
import 'package:budget_planner_app/utils/size_config.dart';
import 'package:budget_planner_app/widgets/item_listview.dart';
import 'package:budget_planner_app/widgets/my_contianer_logo.dart';
import 'package:budget_planner_app/widgets/my_text.dart';
import 'package:budget_planner_app/widgets/my_text_button.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:grouped_list/grouped_list.dart';
import 'package:percent_indicator/circular_percent_indicator.dart';
import 'package:provider/provider.dart';

class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  bool check = false;
  bool visible = true;
  List<Currency> _listCurrency = [];

  @override
  void initState() {
    Provider.of<ActionsChangeNotifierController>(context, listen: false).read();
    Provider.of<ActionsChangeNotifierController>(context, listen: false)
        .readLastActions();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    _listCurrency =
        Provider.of<CurrencyChangeNotifierController>(context).listCurrency;

    return Container(
      color: AppColors.WHITE,
      padding: EdgeInsets.symmetric(horizontal: SizeConfig.scaleWidth(20)),
      child: Column(
        children: [
          SizedBox(height: SizeConfig.scaleHeight(20)),
          Visibility(
            visible: 1 -
                        (Provider.of<ActionsChangeNotifierController>(context,
                                    listen: false)
                                .wallet2 /
                            SharedPrefController().getUser().dayLimit) <
                    .2 &&
                visible,
            child: Container(
              height: SizeConfig.scaleHeight(70),
              decoration: BoxDecoration(
                color: AppColors.WHITE,
                borderRadius: BorderRadius.circular(8),
                boxShadow: [
                  BoxShadow(
                    color: AppColors.SHADOW,
                    blurRadius: 18,
                    offset: Offset(0, 10),
                  ),
                ],
              ),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  SizedBox(width: SizeConfig.scaleWidth(20)),
                  Icon(Icons.error_outline,
                      color: AppColors.RED_TEXT_LISTVIEW, size: 24),
                  SizedBox(width: SizeConfig.scaleWidth(10)),
                  Expanded(
                    child: MyText(
                      title: AppLocalizations.of(context)!.limitToday,
                      fontSize: 15,
                      textAlign: TextAlign.start,
                      fontWeight: FontWeight.w500,
                      color: AppColors.Label_TextFiled,
                    ),
                  ),
                  Container(
                    alignment: Alignment.center,
                    decoration: BoxDecoration(
                      color: AppColors.BG_ICON_CLOSE,
                      shape: BoxShape.circle,
                    ),
                    child: Material(
                      borderRadius: BorderRadius.circular(20),
                      color: AppColors.BG_ICON_CLOSE,
                      child: InkWell(
                        onTap: () {
                          visible = false;
                          setState(() {});
                        },
                        child: Icon(
                          Icons.close,
                          color: AppColors.WHITE,
                          size: 16,
                        ),
                      ),
                    ),
                  ),
                  SizedBox(width: SizeConfig.scaleWidth(13)),
                ],
              ),
            ),
          ),
          SizedBox(height: SizeConfig.scaleHeight(20)),
          MyContianerLogo(
            height: SizeConfig.scaleHeight(260),
            width: 260,
            bg_Container: AppColors.WHITE,
            paddingVertical: 0,
            paddingHorizontal: 0,
            boxShape: BoxShape.circle,
            enableRadius: false,
            shadow: AppColors.SHADOW3,
            blur: 11,
            xOffset: 0,
            yOffset: 13,
            child: CircularPercentIndicator(
              radius: SizeConfig.scaleHeight(260),
              backgroundColor: Colors.transparent,
              lineWidth: 13,
              percent: _getPercent(),
              progressColor: AppColors.BG_BUTTON,
              circularStrokeCap: CircularStrokeCap.round,
              animation: true,
              center: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  SizedBox(height: SizeConfig.scaleHeight(46)),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      MyText(
                        title: Provider.of<ActionsChangeNotifierController>(
                                context)
                            .wallet2
                            .toString(),
                        fontSize: 40,
                        height: 1,
                        fontWeight: FontWeight.w600,
                      ),
                      SizedBox(
                        width: SizeConfig.scaleWidth(4),
                      ),
                      MyText(
                        title: _getCurrency(),
                        fontSize: 16,
                        fontWeight: FontWeight.w600,
                        height: 1,
                      ),
                    ],
                  ),
                  SizedBox(height: SizeConfig.scaleHeight(12)),
                  MyText(
                    title: AppLocalizations.of(context)!.spentToday,
                    fontSize: 16.5,
                    fontWeight: FontWeight.w400,
                    color: AppColors.GRAY,
                    height: 1,
                  ),
                  SizedBox(height: SizeConfig.scaleHeight(14)),
                  Divider(
                    endIndent: SizeConfig.scaleWidth(33),
                    indent: SizeConfig.scaleWidth(33),
                    color: AppColors.DIVIDER,
                  ),
                  SizedBox(height: SizeConfig.scaleHeight(14)),
                  MyText(
                    title: AppLocalizations.of(context)!.balanceToday,
                    fontSize: 16.5,
                    fontWeight: FontWeight.w400,
                    height: 1,
                    color: AppColors.GRAY,
                  ),
                  SizedBox(
                    height: SizeConfig.scaleHeight(8),
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      MyText(
                        title: Provider.of<ActionsChangeNotifierController>(
                                context)
                            .wallet
                            .toString(),
                        fontSize: 23,
                        fontWeight: FontWeight.w600,
                        color: AppColors.GREEN,
                        height: 1,
                      ),
                      SizedBox(
                        width: SizeConfig.scaleWidth(4),
                      ),
                      MyText(
                        title: _getCurrency(),
                        fontSize: 13,
                        fontWeight: FontWeight.w600,
                        color: AppColors.GREEN,
                        height: 1,
                      ),
                    ],
                  ),
                ],
              ),
            ),
          ),
          SizedBox(height: SizeConfig.scaleHeight(40)),
          SizedBox(
            width: double.infinity,
            child: MyText(title: AppLocalizations.of(context)!.lastActions),
          ),
          Expanded(
            child: MyContianerLogo(
              marginTop: 20,
              marginBottom: 20,
              paddingVertical: 15,
              paddingHorizontal: 15,
              bg_Container: AppColors.WHITE,
              xOffset: 0,
              yOffset: 5,
              blur: 18,
              shadow: AppColors.BLACK.withOpacity(.16),
              width: double.infinity,
              borderRadius: 8,
              child: Column(
                children: [
                  Expanded(
                    child: Consumer<ActionsChangeNotifierController>(
                      builder: (
                        BuildContext context,
                        ActionsChangeNotifierController value,
                        Widget? child,
                      ) {
                        return value.listLastAction.length > 0
                            ? GroupedListView<CategoryActions, String>(
                                elements: value.listLastAction,
                                groupBy: (CategoryActions element) =>
                                    element.date,
                                groupSeparatorBuilder: (String groupByValue) {
                                  return MyText(
                                    title: groupByValue,
                                    height: 2,
                                    fontSize: 12,
                                    color: AppColors.TEXT_NOTE,
                                  );
                                },
                                itemBuilder:
                                    (context, CategoryActions element) {
                                  return ItemListview(
                                    context: context,
                                    actions: element,
                                    listCurrency: _listCurrency,
                                  );
                                },
                                itemComparator: (CategoryActions item1,
                                    CategoryActions item2) {
                                  return item1.date.compareTo(item2.date);
                                },
                                useStickyGroupSeparators: true,
                                floatingHeader: true,
                                order: GroupedListOrder.DESC,
                                separator: Divider(),
                              )
                            : Center(
                                child: MyText(
                                  title: AppLocalizations.of(context)!.noData,
                                ),
                              );
                      },
                    ),
                  ),
                  MyTextButton(
                    title: AppLocalizations.of(context)!.seeMore,
                    onPreesed: () {
                      // TODO:: NAVIGATOR TO ACTIONS SCREEN
                      Navigator.pushNamed(context, '/actions_screen');
                    },
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }

  double _getPercent() {
    double percent = 1 -
        (Provider.of<ActionsChangeNotifierController>(context).wallet2 /
            SharedPrefController().getUser().dayLimit);
    if (percent <= .2) visible = true;
    setState(() {});
    return percent;
  }

  String _getCurrency() {
    var currencies =
        Provider.of<CurrencyChangeNotifierController>(context, listen: false)
            .listCurrency;
    String currency = '';
    if (currencies.isNotEmpty) {
      Currency object = currencies[
          Provider.of<UserChangeNotifierController>(context)
              .getUser()
              .currencyId];
      if (SharedPrefController().getValueLang == 'en') {
        currency = object.nameEn;
      } else {
        currency = object.nameAr;
      }
    }
    return currency;
  }
}

/*
*

*
* */
