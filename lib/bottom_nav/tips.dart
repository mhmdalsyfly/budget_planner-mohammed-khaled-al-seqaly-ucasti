import 'package:budget_planner_app/utils/app_colors.dart';
import 'package:budget_planner_app/utils/size_config.dart';
import 'package:budget_planner_app/widgets/item_tips.dart';
import 'package:budget_planner_app/widgets/my_contianer_logo.dart';
import 'package:budget_planner_app/widgets/my_searchview.dart';
import 'package:budget_planner_app/model/tips.dart';
import 'package:budget_planner_app/widgets/my_text.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class Tips extends StatefulWidget {
  const Tips({Key? key}) : super(key: key);

  @override
  _TipsState createState() => _TipsState();
}

class _TipsState extends State<Tips> {
  late TextEditingController _search;
  List<Tipss> _listTips = [];

  @override
  void initState() {
    _search = TextEditingController();
    super.initState();
  }

  @override
  void didChangeDependencies() {
    _listTips = [
      Tipss(
          date: '3 month ago',
          title: AppLocalizations.of(context)!.tips1,
          subject: AppLocalizations.of(context)!.subject1,
          image: 'images/on_boarding/image1.png'),
      Tipss(
          date: '12 month ago',
          title: AppLocalizations.of(context)!.tips2,
          subject: AppLocalizations.of(context)!.subject2,
          image: 'images/on_boarding/image2.png'),
      Tipss(
          date: '3 month ago',
          subject: AppLocalizations.of(context)!.subject3,
          title: AppLocalizations.of(context)!.tips3,
          image: 'images/on_boarding/image3.png'),
      Tipss(
          date: '12 month ago',
          subject: AppLocalizations.of(context)!.subject4,
          title: AppLocalizations.of(context)!.tips4,
          image: 'images/on_boarding/image1.png'),
    ];
    super.didChangeDependencies();
  }

  @override
  void dispose() {
    _search.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      color: AppColors.BG_PROGRESS,
      height: double.infinity,
      width: double.infinity,
      child: Padding(
        padding: EdgeInsets.symmetric(horizontal: SizeConfig.scaleWidth(20)),
        child: Column(
          children: [
            SizedBox(height: SizeConfig.scaleHeight(35)),
            Container(
              height: SizeConfig.scaleHeight(55),
              decoration: BoxDecoration(
                color: AppColors.WHITE,
                borderRadius: BorderRadius.circular(8),
              ),
              child: MySearchView(
                searchViewController: _search,
              ),
            ),
            SizedBox(height: SizeConfig.scaleHeight(27)),
            Expanded(
              child: GridView.builder(
                gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                    crossAxisCount: 2,
                    childAspectRatio: 172 / 230,
                    crossAxisSpacing: 10,
                    mainAxisSpacing: 17),
                itemCount: _listTips.length,
                itemBuilder: (context, index) {
                  return ItemTips(_listTips[index],index);
                },
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget myTextFieldSearch() {
    return Expanded(
      child: TextField(
        textAlign: TextAlign.start,
        controller: _search,
        decoration: InputDecoration(
          focusedBorder: InputBorder.none,
          enabledBorder: InputBorder.none,
          hintText: AppLocalizations.of(context)!.search,
          hintStyle: TextStyle(
            color: AppColors.HINT_TEXT,
            fontFamily: 'Montserrat',
            fontWeight: FontWeight.w400,
            fontSize: SizeConfig.scaleTextFont(15),
          ),
        ),
      ),
    );
  }
}

