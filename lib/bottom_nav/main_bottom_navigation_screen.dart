import 'package:budget_planner_app/bottom_nav/categories.dart';
import 'package:budget_planner_app/bottom_nav/home.dart';
import 'package:budget_planner_app/bottom_nav/profile.dart';
import 'package:budget_planner_app/bottom_nav/tips.dart';
import 'package:budget_planner_app/model/content_bottom_nav.dart';
import 'package:budget_planner_app/model/currency.dart';
import 'package:budget_planner_app/provider_notify/actions_change_notifyer_controller.dart';
import 'package:budget_planner_app/provider_notify/category_change_notifyer_controller.dart';
import 'package:budget_planner_app/utils/app_colors.dart';
import 'package:budget_planner_app/utils/size_config.dart';
import 'package:budget_planner_app/widgets/my_text.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:provider/provider.dart';

class MainBottomNavigationScreen extends StatefulWidget {
  @override
  _MainBottomNavigationScreenState createState() =>
      _MainBottomNavigationScreenState();
}

class _MainBottomNavigationScreenState
    extends State<MainBottomNavigationScreen> {
  int _currentPage = 0;
  Color _colorItem1 = AppColors.SELECTED_ITEM;
  Color _colorItem2 = AppColors.UN_SELECTED_ITEM;
  Color _colorItem3 = AppColors.UN_SELECTED_ITEM;
  Color _colorItem4 = AppColors.UN_SELECTED_ITEM;

  List<ContentBottomNav> _list = [];

  @override
  void initState() {
    Provider.of<CategoryChangeNotifierController>(context, listen: false)
        .readCategoryExpenses();
    Provider.of<CategoryChangeNotifierController>(context, listen: false)
        .readCategoryIncome();

    super.initState();
  }

  @override
  void didChangeDependencies() {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
      statusBarColor:
          _currentPage != 3 ? AppColors.WHITE : AppColors.BG_PROGRESS,
      statusBarBrightness: Brightness.dark,
    ));

    _list = [
      ContentBottomNav(
          title: AppLocalizations.of(context)!.home, screen: Home()),
      ContentBottomNav(
          title: AppLocalizations.of(context)!.categories,
          screen: Categories()),
      ContentBottomNav(
          title: AppLocalizations.of(context)!.profile, screen: Profile()),
      ContentBottomNav(
          title: AppLocalizations.of(context)!.tips, screen: Tips()),
    ];

    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        brightness: Brightness.light,
        backgroundColor:
            _currentPage != 3 ? AppColors.WHITE : AppColors.BG_PROGRESS,
        elevation: 0,
        title: MyText(
          title: _list[_currentPage].title,
        ),
        actions: [
          Container(
            margin: EdgeInsetsDirectional.only(end: SizeConfig.scaleWidth(20)),
            child: trellingIcon(),
          )
        ],
      ),
      body: _list[_currentPage].screen,
      bottomNavigationBar: Container(
        height: SizeConfig.scaleHeight(70),
        decoration: BoxDecoration(
          border: Border.all(color: AppColors.BG_BUTTON_GRAY, width: .3),
          boxShadow: [
            BoxShadow(
              offset: Offset(0, -.5),
              color: AppColors.BG_BOTTOM_NAVIGATION,
            ),
          ],
        ),
        child: Row(
          children: [
            itemBottomNavigation(
              title: AppLocalizations.of(context)!.home,
              IconData: Icons.dashboard_outlined,
              itemColor: _colorItem1,
            ),
            itemBottomNavigation(
              title: AppLocalizations.of(context)!.categories,
              IconData: Icons.category_outlined,
              itemColor: _colorItem2,
            ),
            Spacer(),
            Container(
              height: SizeConfig.scaleHeight(50),
              width: SizeConfig.scaleWidth(50),
              child: FloatingActionButton(
                onPressed: () {
                  _navigatorAddOperation();
                },
                elevation: 0,
                backgroundColor: AppColors.BG_BUTTON,
                child: Icon(Icons.add),
              ),
            ),
            Spacer(),
            itemBottomNavigation(
              title: AppLocalizations.of(context)!.profile,
              IconData: Icons.person_rounded,
              itemColor: _colorItem3,
            ),
            itemBottomNavigation(
              title: AppLocalizations.of(context)!.tips,
              IconData: Icons.info_outline,
              itemColor: _colorItem4,
            ),
          ],
        ),
      ),
    );
  }

  Container itemBottomNavigation({
    required String title,
    required IconData,
    required Color itemColor,
  }) {
    getColorBottomNavigationItem();
    return Container(
      width: SizeConfig.scaleWidth(82),
      height: SizeConfig.scaleHeight(50),
      child: Material(
        color: AppColors.BG_BOTTOM_NAVIGATION,
        child: InkWell(
          onTap: () {
            choseScreen(title);
            getColorBottomNavigationItem();
            setState(() {});
          },
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Icon(
                IconData,
                color: itemColor,
                size: 22,
              ),
              MyText(
                title: title,
                fontSize: 10,
                color: itemColor,
              )
            ],
          ),
        ),
      ),
    );
  }

  void getColorBottomNavigationItem() {
    switch (_currentPage) {
      case 0:
        _colorItem1 = AppColors.SELECTED_ITEM;
        _colorItem2 = AppColors.UN_SELECTED_ITEM;
        _colorItem3 = AppColors.UN_SELECTED_ITEM;
        _colorItem4 = AppColors.UN_SELECTED_ITEM;
        break;
      case 1:
        _colorItem1 = AppColors.UN_SELECTED_ITEM;
        _colorItem2 = AppColors.SELECTED_ITEM;
        _colorItem3 = AppColors.UN_SELECTED_ITEM;
        _colorItem4 = AppColors.UN_SELECTED_ITEM;
        break;
      case 2:
        _colorItem1 = AppColors.UN_SELECTED_ITEM;
        _colorItem2 = AppColors.UN_SELECTED_ITEM;
        _colorItem3 = AppColors.SELECTED_ITEM;
        _colorItem4 = AppColors.UN_SELECTED_ITEM;
        break;
      case 3:
        _colorItem1 = AppColors.UN_SELECTED_ITEM;
        _colorItem2 = AppColors.UN_SELECTED_ITEM;
        _colorItem3 = AppColors.UN_SELECTED_ITEM;
        _colorItem4 = AppColors.SELECTED_ITEM;
        break;
    }
  }

  void choseScreen(String title) {
    if (title == AppLocalizations.of(context)!.home) {
      _currentPage = 0;
    } else if (title == AppLocalizations.of(context)!.categories) {
      _currentPage = 1;
    } else if (title == AppLocalizations.of(context)!.profile) {
      _currentPage = 2;
    } else if (title == AppLocalizations.of(context)!.tips) {
      _currentPage = 3;
    }
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
      statusBarColor:
          _currentPage != 3 ? AppColors.WHITE : AppColors.BG_PROGRESS,
      statusBarBrightness: Brightness.dark,
    ));
  }

  Widget? trellingIcon() {
    if (_currentPage == 0) {
      return IconButton(
        icon: Icon(
          Icons.settings,
          color: AppColors.BG_BUTTON,
          size: 24,
        ),
        onPressed: () {
          _navigatiorToSettingsScreen();
        },
        padding: EdgeInsets.zero,
      );
    } else if (_currentPage == 1) {
      return IconButton(
        icon: Icon(
          Icons.add_circle_sharp,
          color: AppColors.BG_BUTTON,
          size: 24,
        ),
        onPressed: () {
          _navigatiorToAddCategoryScreen();
        },
        padding: EdgeInsets.zero,
      );
    } else {
      return null;
    }
  }

  void _navigatiorToSettingsScreen() {
    Navigator.pushNamed(context, '/settings_screen');
  }

  void _navigatiorToAddCategoryScreen() {
    Navigator.pushNamed(context, '/add_category_screen');
  }

  void _navigatorAddOperation() {
    Navigator.pushNamed(context, '/add_operation_screen');
  }
}
