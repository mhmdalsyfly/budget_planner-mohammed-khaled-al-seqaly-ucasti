import 'package:budget_planner_app/helpers/mixin.dart';
import 'package:budget_planner_app/helpers/show_snakbar.dart';
import 'package:budget_planner_app/model/currency.dart';
import 'package:budget_planner_app/model/user.dart';
import 'package:budget_planner_app/provider_notify/actions_change_notifyer_controller.dart';
import 'package:budget_planner_app/provider_notify/currency_change_notifyer_controller.dart';
import 'package:budget_planner_app/provider_notify/user_change_notifyer_controller.dart';
import 'package:budget_planner_app/screens/add_operation_screen.dart';
import 'package:budget_planner_app/screens/pin_code_screen.dart';
import 'package:budget_planner_app/storage/pref/shared_pref_controller.dart';
import 'package:budget_planner_app/utils/app_colors.dart';
import 'package:budget_planner_app/utils/size_config.dart';
import 'package:budget_planner_app/widgets/my_contianer_logo.dart';
import 'package:budget_planner_app/widgets/my_text.dart';
import 'package:budget_planner_app/widgets/my_text_button.dart';
import 'package:budget_planner_app/widgets/my_textfield2.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:provider/provider.dart';

class Profile extends StatefulWidget {
  @override
  _ProfileState createState() => _ProfileState();
}

class _ProfileState extends State<Profile> with Transformation {
  late TextEditingController _nameTextEditing;
  late TextEditingController _emailTextEditing;
  late TextEditingController _currencyNumEditing;
  late TextEditingController _dailyLimitNumEditing;

  Color _bgButtonCreate = AppColors.BG_BUTTON_GRAY;
  String _pinCode = '';
  late Future futureList;
  int? _currencyId;
  late User _myAccount;

  @override
  void initState() {
    super.initState();
  }

  @override
  void didChangeDependencies() {
    _myAccount = Provider.of<UserChangeNotifierController>(context).getUser();
    _nameTextEditing = TextEditingController(text: _myAccount.name);
    _emailTextEditing = TextEditingController(text: _myAccount.email);
    _currencyNumEditing = TextEditingController();
    _currencyId = _myAccount.currencyId;
    print(_currencyId);
    _choseCurrency(_currencyId);
    _dailyLimitNumEditing =
        TextEditingController(text: _myAccount.dayLimit.toString());

    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      height: double.infinity,
      width: double.infinity,
      padding: EdgeInsets.symmetric(
        horizontal: SizeConfig.scaleWidth(20),
      ),
      color: AppColors.WHITE,
      child: SingleChildScrollView(
        child: Column(
          children: [
            SizedBox(height: SizeConfig.scaleHeight(24)),
            MyContianerLogo(
              height: SizeConfig.scaleHeight(115),
              paddingHorizontal: 0,
              paddingVertical: 0,
              alignment: AlignmentDirectional.center,
              bg_Container: AppColors.WHITE,
              child: Image.asset(
                'images/ic_profile_inactive.png',
                fit: BoxFit.fill,
                width: SizeConfig.scaleWidth(52),
                height: SizeConfig.scaleHeight(56),
              ),
            ),
            SizedBox(height: SizeConfig.scaleHeight(13)),
            MyText(
              title: Provider.of<UserChangeNotifierController>(context)
                  .getUser()
                  .name,
            ),
            SizedBox(height: SizeConfig.scaleHeight(21)),
            MyContianerLogo(
              borderRadius: 8,
              xOffset: 0,
              yOffset: 3,
              blur: 6,
              shadow: AppColors.BLACK.withOpacity(.16),
              paddingHorizontal: 17,
              paddingVertical: 0,
              width: double.infinity,
              child: Column(
                children: [
                  MyTextField2(
                    title: AppLocalizations.of(context)!.name,
                    subTitle: AppLocalizations.of(context)!.none,
                    controller: _nameTextEditing,
                    onTap: () {},
                    onChange: (String value) {
                      if (_checkData()) {
                        _bgButtonCreate = AppColors.BG_BUTTON;
                      } else {
                        _bgButtonCreate = AppColors.BG_BUTTON_GRAY;
                      }
                    },
                  ),
                  MyTextField2(
                    title: AppLocalizations.of(context)!.email,
                    subTitle: AppLocalizations.of(context)!.none,
                    controller: _emailTextEditing,
                    textInputType: TextInputType.emailAddress,
                    onTap: () {},
                    onChange: (String value) {
                      if (_checkData()) {
                        _bgButtonCreate = AppColors.BG_BUTTON;
                      } else {
                        _bgButtonCreate = AppColors.BG_BUTTON_GRAY;
                      }
                    },
                  ),
                  MyTextField2(
                    title: AppLocalizations.of(context)!.currency,
                    subTitle: AppLocalizations.of(context)!.dollar,
                    controller: _currencyNumEditing,
                    textInputType: TextInputType.number,
                    check: true,
                    enabled: false,
                    onTap: () {
                      _getDataFromBackCurrencyScreen();
                    },
                  ),
                  MyTextField2(
                    title: AppLocalizations.of(context)!.dailyLimit,
                    subTitle: AppLocalizations.of(context)!.ust,
                    controller: _dailyLimitNumEditing,
                    textInputType: TextInputType.number,
                    onTap: () {},
                    onChange: (String value) {
                      if (_checkData()) {
                        _bgButtonCreate = AppColors.BG_BUTTON;
                      } else {
                        _bgButtonCreate = AppColors.BG_BUTTON_GRAY;
                      }
                    },
                  ),
                  Container(
                    alignment: AlignmentDirectional.centerStart,
                    child: MyTextButton(
                      title: AppLocalizations.of(context)!.changePin,
                      check: false,
                      onPreesed: () {
                        _getPinCodeFromPinCodeScreen();
                      },
                      textColor: AppColors.Label_TextFiled,
                    ),
                  ),
                ],
              ),
            ),
            SizedBox(height: SizeConfig.scaleHeight(25)),
            MyTextButton(
              title: AppLocalizations.of(context)!.save,
              checkBgColor: _bgButtonCreate,
              onPreesed: () {
                _performChangeAccount();
              },
            ),
          ],
        ),
      ),
    );
  }

  void _getDataFromBackCurrencyScreen() async {
    _currencyId =
        await Navigator.pushNamed(context, '/chose_currency_screen') as int;
    _choseCurrency(_currencyId);
    if (_checkData()) {
      _bgButtonCreate = AppColors.BG_BUTTON;
    } else {
      _bgButtonCreate = AppColors.BG_BUTTON_GRAY;
    }
    setState(() {});
  }

  void _getPinCodeFromPinCodeScreen() async {
    _pinCode = await Navigator.push(
          context,
          MaterialPageRoute(
            builder: (context) => PinCodeScreen(
              title: AppLocalizations.of(context)!.enterPasscodeOld,
              pinCodeOld: _myAccount.pin,
            ),
          ),
        ) ??
        '';

    if (_checkData()) {
      _bgButtonCreate = AppColors.BG_BUTTON;
    } else {
      _bgButtonCreate = AppColors.BG_BUTTON_GRAY;
    }
    setState(() {});
  }

  bool _checkData() {
    if (_nameTextEditing.text.trim().isNotEmpty &&
        _emailTextEditing.text.trim().isNotEmpty &&
        _currencyNumEditing.text.trim().isNotEmpty &&
        _dailyLimitNumEditing.text.trim().isNotEmpty &&
        _pinCode.length == 4) {
      return true;
    }
    return false;
  }

  _performChangeAccount() {
    if (_checkData()) {
      //TODO:: CHANGE DATA ACCOUNT
      _changeDataAccount();
    } else {
      Helpers.showSnackBar(
        context,
        message: AppLocalizations.of(context)!.messagePinCode,
        error: true,
      );
    }
  }

  void _changeDataAccount() async {
    if (Provider.of<ActionsChangeNotifierController>(context, listen: false)
            .wallet2 <
        currencyTransformation(
            amount: user.dayLimit.toDouble(), idCurrency: user.currencyId)) {

      bool updated = await Provider.of<UserChangeNotifierController>(context,
              listen: false)
          .update(user);

      Helpers.showSnackBar(
        context,
        message: updated
            ? AppLocalizations.of(context)!.updatedData
            : AppLocalizations.of(context)!.updatedDataField,
        error: !updated,
      );
    } else {
      Helpers.showSnackBar(
        context,
        message: AppLocalizations.of(context)!.dailyEntryLimit,
        error: true,
      );
    }
  }

  User get user {
    return User(
      id: _myAccount.id,
      name: _nameTextEditing.text.trim(),
      currencyId: _currencyId ?? -1,
      dayLimit: int.parse(_dailyLimitNumEditing.text.trim()),
      email: _emailTextEditing.text.trim(),
      pin: int.parse(_pinCode),
    );
  }

  _choseCurrency(int? current) {
    var currencies =
        Provider.of<CurrencyChangeNotifierController>(context, listen: false)
            .listCurrency;

    if (currencies.isNotEmpty) {
      Currency object = currencies[current!];
      if (SharedPrefController().getValueLang == 'en') {
        _currencyNumEditing.text = object.nameEn;
      } else {
        _currencyNumEditing.text = object.nameAr;
      }
    }
  }
}
