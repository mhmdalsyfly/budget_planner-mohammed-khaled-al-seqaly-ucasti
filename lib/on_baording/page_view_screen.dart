import 'package:budget_planner_app/model/content.dart';
import 'package:budget_planner_app/on_baording/on_baording_content.dart';
import 'package:budget_planner_app/on_baording/on_boarding_indicator.dart';
import 'package:budget_planner_app/utils/app_colors.dart';
import 'package:budget_planner_app/utils/size_config.dart';
import 'package:budget_planner_app/widgets/my_text.dart';
import 'package:budget_planner_app/widgets/my_text_button.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:flutter/material.dart';

class PageViewScreen extends StatefulWidget {
  @override
  _PageViewScreenState createState() => _PageViewScreenState();
}

class _PageViewScreenState extends State<PageViewScreen> {
  late PageController _pageController;

  int _currentPage = 0;
  bool check = false;
  double value = .34;

  List<Content> _list = [];

  @override
  void initState() {
    super.initState();
    _pageController = PageController();
  }

  @override
  void didChangeDependencies() {
    _list = [
      Content(
        title: AppLocalizations.of(context)!.take_hold,
        subTitle: AppLocalizations.of(context)!.managingFinances,
        image: 'images/on_boarding/image1.png',
      ),
      Content(
        title: AppLocalizations.of(context)!.seeMoney,
        subTitle: AppLocalizations.of(context)!.appHelps,
        image: 'images/on_boarding/image2.png',
      ),
      Content(
        title: AppLocalizations.of(context)!.reachGoals,
        subTitle: AppLocalizations.of(context)!.effortlessBudget,
        image: 'images/on_boarding/image3.png',
      ),
    ];
    super.didChangeDependencies();
  }

  @override
  void dispose() {
    super.dispose();
    _pageController.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Stack(
        children: [
          PageView.builder(
            controller: _pageController,
            onPageChanged: (int currentPage) {
              setState(() {
                _currentPage = currentPage;
                if (_currentPage == 0) {
                  value = .3;
                } else if (_currentPage == 1) {
                  value = .65;
                } else if (_currentPage == 2) {
                  value = 1;
                }
              });
            },
            itemCount: 3,
            itemBuilder: (context, index) {
              return OnBoardingContent(
                content: _list[index],
              );
            },
          ),
          Positioned(
            bottom: SizeConfig.scaleHeight(84),
            right: 20,
            left: 20,
            child: MyTextButton(
              title: _currentPage == _list.length - 1
                  ? AppLocalizations.of(context)!.letsStart
                  : AppLocalizations.of(context)!.next,
              onPreesed: () {
                _pageController.nextPage(
                  duration: Duration(milliseconds: 700),
                  curve: Curves.easeIn,
                );
                if (_currentPage == _list.length - 1) {
                  // TODO: GO TO HOME SCREEN
                  Navigator.pushReplacementNamed(context, '/login_screen');
                }
              },
            ),
          ),
          Positioned(
            bottom: SizeConfig.scaleHeight(22),
            right: 20,
            left: 20,
            child: MyTextButton(
              title: AppLocalizations.of(context)!.skip,
              onPreesed: () {
                _pageController.jumpToPage(_list.length - 1);
              },
              check: false,
              textColor: AppColors.GRAY,
            ),
          ),
          Positioned(
            top: SizeConfig.scaleHeight(62),
            right: SizeConfig.scaleWidth(107),
            left: SizeConfig.scaleWidth(107),
            child: Container(
              child: ClipRRect(
                borderRadius: BorderRadius.circular(5),
                child: LinearProgressIndicator(
                  backgroundColor: AppColors.BG_PROGRESS,
                  value: value,
                  valueColor:
                      new AlwaysStoppedAnimation<Color>(AppColors.BG_BUTTON),
                  minHeight: 10,
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
