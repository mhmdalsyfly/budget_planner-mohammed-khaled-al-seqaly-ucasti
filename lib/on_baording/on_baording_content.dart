import 'package:budget_planner_app/model/content.dart';
import 'package:budget_planner_app/utils/size_config.dart';
import 'package:budget_planner_app/widgets/my_text.dart';
import 'package:flutter/material.dart';

class OnBoardingContent extends StatefulWidget {
  Content content;

  OnBoardingContent({required this.content});

  @override
  _OnBoardingContentState createState() => _OnBoardingContentState();
}

class _OnBoardingContentState extends State<OnBoardingContent> {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Padding(
        padding: EdgeInsets.symmetric(
          horizontal: SizeConfig.scaleWidth(20),
        ),
        child: Column(
          children: [
            SizedBox(height: SizeConfig.scaleHeight(117)),
            MyText(
              title: widget.content.title,
              height: 1.5,
            ),
            SizedBox(height: SizeConfig.scaleHeight(11)),
            MyText(
              title: widget.content.subTitle,
              fontWeight: FontWeight.w400,
              fontSize: 15,
              height: 1.5,
            ),
            SizedBox(height: SizeConfig.scaleHeight(90)),
            Image.asset(
              widget.content.image,
              height: SizeConfig.scaleHeight(310),
              width: SizeConfig.scaleWidth(274),
            ),
          ],
        ),
      ),
    );
  }
}
