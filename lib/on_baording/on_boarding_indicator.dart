import 'package:budget_planner_app/utils/app_colors.dart';
import 'package:budget_planner_app/utils/size_config.dart';
import 'package:flutter/material.dart';

class OnBoardingIndicator extends StatelessWidget {
  bool selected;

  OnBoardingIndicator({
    required this.selected,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      height: SizeConfig.scaleHeight(10),
      width: SizeConfig.scaleWidth(66),
      decoration: BoxDecoration(
        color: selected ? AppColors.BG_BUTTON : AppColors.BG_PROGRESS,
        borderRadius: BorderRadius.circular(7),
      ),
    );
  }
}
