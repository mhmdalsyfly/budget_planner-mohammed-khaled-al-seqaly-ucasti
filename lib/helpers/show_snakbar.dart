import 'package:budget_planner_app/utils/app_colors.dart';
import 'package:budget_planner_app/widgets/my_text.dart';
import 'package:flutter/material.dart';

class Helpers {
  static showSnackBar(
    BuildContext context, {
    required String message,
    bool error = false,
  }) {
    ScaffoldMessenger.of(context).showSnackBar(
      SnackBar(
        content: MyText(
          title: message,
          fontWeight: FontWeight.w500,
          fontSize: 16,
          color: AppColors.WHITE,
        ),
        elevation: 4,
        backgroundColor: error ? AppColors.RED : AppColors.GREEN,
      ),
    );
  }
}
