import 'package:budget_planner_app/storage/pref/shared_pref_controller.dart';

mixin Transformation {
  double currencyTransformation({
    required double amount,
    required int idCurrency,
  }) {
    double conversion = amount;
    int currencyUser = SharedPrefController().getUser().currencyId;

    if (idCurrency == 0 && currencyUser == idCurrency) {
      // العملة اللي معي واللي اجتني شيكل
      conversion = amount;
    } else if (idCurrency == 1 && currencyUser == idCurrency) {
      // العملة اللي معي واللي اجتني دولار
      conversion = amount;
    } else if (idCurrency == 2 && currencyUser == idCurrency) {
      // العملة اللي معي واللي اجتني دينار
      conversion = amount;
    } else if (currencyUser == 0 && idCurrency == 1) {
      // العملة اللي اجتني دولار ونا معي شيكل
      conversion = amount * 3.27;
    } else if (currencyUser == 0 && idCurrency == 2) {
      // العلمة اللي اجتني دينار ونا معي شيكل
      conversion = amount * 4.62;
    } else if (currencyUser == 1 && idCurrency == 0) {
      // العملة اللي اجتني شيكل ونا معي دولار
      conversion = amount / 3.27;
    } else if (currencyUser == 1 && idCurrency == 2) {
      // العلمة اللي اجتني دينار ونا معي دولار
      conversion = amount * 1.41;
    } else if (currencyUser == 2 && idCurrency == 0) {
      // العملة اللي اجتني شيكل ونا معي دينار
      conversion = amount / 4.62;
    } else if (currencyUser == 2 && idCurrency == 1) {
      // العلمة اللي احتني دولار ونا معي دينار
      conversion = amount / 1.41;
    }
    return conversion;
  }
}
