import 'dart:convert';

import 'package:budget_planner_app/model/user.dart';
import 'package:budget_planner_app/storage/pref/pref_keys.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SharedPrefController {
  static final SharedPrefController _instance =
      SharedPrefController._internal();

  late SharedPreferences _preferences;

  SharedPrefController._internal();

  factory SharedPrefController() => _instance;

  Future<SharedPreferences> initSharedPref() async =>
      _preferences = await SharedPreferences.getInstance();

  SharedPreferences get prefManager => _preferences;

  Future<bool> setValueLang(String value) async =>
      await _preferences.setString(PrefKeys.LANG_CODE_KEY, value);

  String get getValueLang =>
      _preferences.getString(PrefKeys.LANG_CODE_KEY) ?? 'en';

  Future<void> setUser(User user) async {
    await _preferences.setInt(PrefKeys.ID, user.id!);
    await _preferences.setString(PrefKeys.NAME, user.name);
    await _preferences.setString(PrefKeys.EMAIL_ACCOUNT, user.email);
    await _preferences.setInt(PrefKeys.CURRENCY_ID, user.currencyId);
    await _preferences.setInt(PrefKeys.DAY_LIMIT, user.dayLimit);
    await _preferences.setInt(PrefKeys.PIN_CODE, user.pin);
  }

  User getUser() {
    return User(
      id: _preferences.getInt(PrefKeys.ID) ?? -1,
      name: _preferences.getString(PrefKeys.NAME) ?? '',
      email: _preferences.getString(PrefKeys.EMAIL_ACCOUNT) ?? '',
      currencyId: _preferences.getInt(PrefKeys.CURRENCY_ID) ?? -1,
      dayLimit: _preferences.getInt(PrefKeys.DAY_LIMIT) ?? -1,
      pin: _preferences.getInt(PrefKeys.PIN_CODE) ?? -1,
    );
  }
}
