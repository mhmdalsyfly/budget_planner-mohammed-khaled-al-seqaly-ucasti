
class PrefKeys {
  static const String LANG_CODE_KEY = 'langCode';
  static const String ID = 'id';
  static const String NAME = 'name';
  static const String EMAIL_ACCOUNT = 'email';
  static const String CURRENCY_ID = 'currency_id';
  static const String PIN_CODE = 'pinCode';
  static const String DAY_LIMIT = 'day_limit';
}
