import 'package:budget_planner_app/model/user.dart';
import 'package:budget_planner_app/storage/db/db_const.dart';
import 'package:budget_planner_app/storage/db/db_operations.dart';
import 'package:budget_planner_app/storage/db/db_provider.dart';
import 'package:budget_planner_app/storage/pref/shared_pref_controller.dart';
import 'package:sqflite/sqflite.dart';

class UserController extends DBOperations<User> {
  Database _database;

  UserController() : _database = DBProvider().database;

  @override
  Future<int> create(User user) async {
    int row = await _database.insert(DBConst.TABLE_USER, user.toMap());
    return row;
  }

  @override
  Future<bool> delete(int id) async {
    return await _database.delete(
          DBConst.TABLE_USER,
          where: 'id=?',
          whereArgs: [id],
        ) >
        0;
  }

  @override
  Future<List<User>> read() async {
    List<Map<String, Object?>> listMap =
        await _database.query(DBConst.TABLE_USER);
    return listMap.map((rowUserMap) => User.fromMap(rowUserMap)).toList();
  }

  @override
  Future<bool> update(User user) async {
    int row = await _database.update(DBConst.TABLE_USER, user.toMap(),
        where: 'id = ?', whereArgs: [user.id]);
    return row != 0;
  }

  Future<bool> clearDataActions() async {
    int clearAction = await _database.delete(DBConst.TABLE_ACTIONS,
        where: 'user_id = ?', whereArgs: [SharedPrefController().getUser().id]);
    return clearAction > 0;
  }

  @override
  Future<List<User>> readCategoryExpenses() {
    // TODO: implement readCategoryExpenses
    throw UnimplementedError();
  }

  @override
  Future<List<User>> readCategoryIncome() {
    // TODO: implement readCategoryIncome
    throw UnimplementedError();
  }
}
