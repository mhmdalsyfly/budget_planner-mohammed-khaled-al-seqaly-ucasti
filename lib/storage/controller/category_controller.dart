import 'package:budget_planner_app/model/category.dart';
import 'package:budget_planner_app/model/category_actions.dart';
import 'package:budget_planner_app/model/user.dart';
import 'package:budget_planner_app/storage/db/db_const.dart';
import 'package:budget_planner_app/storage/db/db_operations.dart';
import 'package:budget_planner_app/storage/db/db_provider.dart';
import 'package:budget_planner_app/storage/pref/shared_pref_controller.dart';
import 'package:sqflite/sqflite.dart';

class CategoryController extends DBOperations<Category> {
  Database _database;

  CategoryController() : _database = DBProvider().database;

  @override
  Future<int> create(Category category) async {
    int row =
        await _database.insert(DBConst.TABLE_CATRGORIES, category.toMap());
    return row;
  }

  @override
  Future<bool> delete(int id) async {
    return await _database
            .delete(DBConst.TABLE_CATRGORIES, where: 'id=?', whereArgs: [id]) >
        0;
  }

  Future<bool> clearDataUser() async {
    return await _database.delete(DBConst.TABLE_CATRGORIES,
            where: 'user_id=?',
            whereArgs: [SharedPrefController().getUser().id]) >
        0;
  }

  @override
  Future<List<Category>> read() async {
    List<Map<String, Object?>> listMap = await _database.query(
        DBConst.TABLE_CATRGORIES,
        where: 'id=?',
        whereArgs: [SharedPrefController().getUser().id]);
    return listMap.map((rowUserMap) => Category.fromMap(rowUserMap)).toList();
  }

  @override
  Future<bool> update(Category category) async {
    int row = await _database.update(DBConst.TABLE_CATRGORIES, category.toMap(),
        where: 'id = ?', whereArgs: [category.id]);
    return row != 0;
  }

  @override
  Future<List<Category>> readCategoryIncome() async {
    List<Map<String, Object?>> listMap = await _database.rawQuery(
        'SELECT categories.id, categories.user_id, categories.name,categories.expense, count(actions.id) AS count_action '
        'FROM categories '
        'LEFT JOIN actions '
        'ON categories.id = actions.category_id '
        'WHERE categories.expense = 0 and categories.user_id = ${SharedPrefController().getUser().id} '
        'GROUP BY categories.id');
    return listMap.map((rowUserMap) => Category.fromMap(rowUserMap)).toList();
  }

  @override
  Future<List<Category>> readCategoryExpenses() async {
    List<Map<String, Object?>> listMap = await _database.rawQuery(
        'SELECT categories.id, categories.user_id, categories.name,categories.expense, count(actions.id) AS count_action '
        'FROM categories '
        'LEFT JOIN actions '
        'ON categories.id = actions.category_id '
        'WHERE categories.expense = 1 and categories.user_id = ${SharedPrefController().getUser().id} '
        'GROUP BY categories.id');
    return listMap.map((rowUserMap) => Category.fromMap(rowUserMap)).toList();
  }

// @override
// Future<List<Category>> readCategoryExpenses() async {
//   List<Map<String, Object?>> listMap = await _database
//       .query(DBConst.TABLE_CATRGORIES, where: 'expense = ?', whereArgs: [1]);
//   return listMap.map((rowUserMap) => Category.fromMap(rowUserMap)).toList();
// }
//
// @override
// Future<List<Category>> readCategoryIncome() async {
//   List<Map<String, Object?>> listMap = await _database
//       .query(DBConst.TABLE_CATRGORIES, where: 'expense = ?', whereArgs: [0]);
//   return listMap.map((rowUserMap) => Category.fromMap(rowUserMap)).toList();
// }
}
