import 'package:budget_planner_app/model/currency.dart';
import 'package:budget_planner_app/storage/db/db_const.dart';
import 'package:budget_planner_app/storage/db/db_operations.dart';
import 'package:budget_planner_app/storage/db/db_provider.dart';
import 'package:sqflite/sqflite.dart';

class CurrencyController extends DBOperations<Currency> {
  Database _database;

  CurrencyController() : _database = DBProvider().database;

  @override
  Future<List<Currency>> read() async {
    List<Map<String, Object?>> listMap = await _database.query(
        DBConst.TABLE_CURRENCIES);

    return listMap.map((rowUserMap) => Currency.fromMap(rowUserMap)).toList();
  }

  @override
  Future<int> create(Currency object) {
    // TODO: implement create
    throw UnimplementedError();
  }

  @override
  Future<bool> delete(int id) {
    // TODO: implement delete
    throw UnimplementedError();
  }

  @override
  Future<List<Currency>> readCategoryExpenses() {
    // TODO: implement readCategoryExpenses
    throw UnimplementedError();
  }

  @override
  Future<List<Currency>> readCategoryIncome() {
    // TODO: implement readCategoryIncome
    throw UnimplementedError();
  }

  @override
  Future<bool> update(Currency object) {
    // TODO: implement update
    throw UnimplementedError();
  }

}
