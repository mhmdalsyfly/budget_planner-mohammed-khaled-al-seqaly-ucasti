import 'package:budget_planner_app/model/category_actions.dart';
import 'package:budget_planner_app/storage/db/db_const.dart';
import 'package:budget_planner_app/storage/db/db_operations.dart';
import 'package:budget_planner_app/storage/db/db_provider.dart';
import 'package:budget_planner_app/storage/pref/shared_pref_controller.dart';
import 'package:sqflite/sqflite.dart';

class ActionController extends DBOperations<CategoryActions> {
  Database _database;

  ActionController() : _database = DBProvider().database;

  @override
  Future<int> create(CategoryActions actions) async {
    int row = await _database.insert(DBConst.TABLE_ACTIONS, actions.toMap());
    return row;
  }

  @override
  Future<bool> delete(int id) async {
    return await _database
            .delete(DBConst.TABLE_ACTIONS, where: 'id=?', whereArgs: [id]) >
        0;
  }

  @override
  Future<List<CategoryActions>> read() async {
    print(SharedPrefController().getUser().id.toString());
    List<Map<String, Object?>> listMap = await _database.query(
        DBConst.TABLE_ACTIONS,
        where: 'user_id=?',
        whereArgs: [SharedPrefController().getUser().id]);
    return listMap
        .map((rowUserMap) => CategoryActions.fromMap(rowUserMap))
        .toList();
  }

  @override
  Future<List<CategoryActions>> readLastActions() async {
    List<Map<String, Object?>> listMap = await _database.query(
        DBConst.TABLE_ACTIONS,
        where: 'user_id=?',
        whereArgs: [SharedPrefController().getUser().id],
        orderBy: 'date DESC',
        limit: 4);

    return listMap
        .map((rowUserMap) => CategoryActions.fromMap(rowUserMap))
        .toList();
  }

  @override
  Future<bool> update(CategoryActions actions) async {
    int row = await _database.update(DBConst.TABLE_ACTIONS, actions.toMap(),
        where: 'id = ?', whereArgs: [actions.id]);
    return row != 0;
  }

  @override
  Future<List<CategoryActions>> readCategoryExpenses() {
    // TODO: implement readCategoryExpenses
    throw UnimplementedError();
  }

  @override
  Future<List<CategoryActions>> readCategoryIncome() {
    // TODO: implement readCategoryIncome
    throw UnimplementedError();
  }
}
