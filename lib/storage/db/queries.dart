import 'package:budget_planner_app/storage/db/db_const.dart';
import 'package:sqflite/sqflite.dart';

mixin Queries {
  late Database dbInstance;

  Future<void> createCurrenciesTable() async {
    await dbInstance.execute('CREATE TABLE ${DBConst.TABLE_CURRENCIES} ('
        'id INTEGER PRIMARY KEY AUTOINCREMENT,'
        'name_en TEXT,'
        'name_ar TEXT'
        ')');
  }

  Future<void> insertCurrencies() async {
    await dbInstance.rawInsert(
        'INSERT INTO ${DBConst.TABLE_CURRENCIES} (name_en, name_ar) VALUES (?, ?)',
        ['NIS', 'شيكل']);

    await dbInstance.rawInsert(
        'INSERT INTO ${DBConst.TABLE_CURRENCIES} (name_en, name_ar) VALUES (?, ?)',
        ['Dollar', 'دولار']);

    await dbInstance.rawInsert(
        'INSERT INTO ${DBConst.TABLE_CURRENCIES} (name_en, name_ar) VALUES (?, ?)',
        ['JOD', 'دينار']);
  }

  Future<void> createUsersTable() async {
    await dbInstance.execute('CREATE TABLE ${DBConst.TABLE_USER} ('
        'id INTEGER PRIMARY KEY AUTOINCREMENT,'
        'name TEXT,'
        'email TEXT UNIQUE,'
        'currency_id INTEGER,'
        'day_limit NUMERIC,'
        'pin INTEGER'
        ')');
  }

  Future<void> createCategoriesTable() async {
    await dbInstance.execute('CREATE TABLE ${DBConst.TABLE_CATRGORIES} ('
        'id INTEGER PRIMARY KEY AUTOINCREMENT,'
        'user_id INTEGER,'
        'name TEXT,'
        'expense BOOLEAN'
        ')');
  }

  Future<void> createActionsTable() async {
    await dbInstance.execute('CREATE TABLE ${DBConst.TABLE_ACTIONS} ('
        'id INTEGER PRIMARY KEY AUTOINCREMENT,'
        'amount INTEGER,'
        'date TEXT,'
        'expense BOOLEAN,'
        'category Text,'
        'notes TEXT NULL,'
        'user_id INTEGER,'
        'category_id INTEGER,'
        'currency_id INTEGER,'
        ' FOREIGN KEY (id) REFERENCES categories(id)'
        ')');
  }
}
