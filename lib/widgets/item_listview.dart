import 'package:budget_planner_app/model/category_actions.dart';
import 'package:budget_planner_app/model/currency.dart';
import 'package:budget_planner_app/provider_notify/currency_change_notifyer_controller.dart';
import 'package:budget_planner_app/screens/expense_details_screen.dart';
import 'package:budget_planner_app/storage/pref/shared_pref_controller.dart';
import 'package:budget_planner_app/utils/app_colors.dart';
import 'package:budget_planner_app/utils/size_config.dart';
import 'package:budget_planner_app/widgets/my_contianer_logo.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';

import 'my_text.dart';

class ItemListview extends StatelessWidget {
  CategoryActions actions;
  List<Currency> listCurrency;
  BuildContext context;

  ItemListview({
    required this.actions,
    required this.context,
    required this.listCurrency,
  });

  @override
  Widget build(BuildContext context) {
    return Material(
      color: AppColors.WHITE,
      child: InkWell(
        onTap: () {
          Navigator.push(
            context,
            MaterialPageRoute(
              builder: (context) => ExpenseDetailsScreen(actions: actions),
            ),
          );
        },
        child: Container(
          height: SizeConfig.scaleHeight(75),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              MyContianerLogo(
                child: MyText(
                  title: actions.category.substring(0, 1).toUpperCase(),
                ),
                height: 54,
                width: 54,
                marginEnd: 11,
                paddingVertical: 0,
                paddingHorizontal: 0,
                alignment: AlignmentDirectional.center,
                borderRadius: 16,
                enableShadow: false,
                bg_Container: AppColors.BG_PROGRESS,
              ),
              Expanded(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    MyText(
                      title: actions.category,
                      fontSize: 15,
                      fontWeight: FontWeight.w600,
                    ),
                    MyText(
                      title: actions.notes ?? '',
                      fontSize: 15,
                      fontWeight: FontWeight.w400,
                      color: AppColors.GRAY,
                    ),
                  ],
                ),
              ),
              MyText(
                title:
                    '${actions.amount.toString()}  ${_getCurrencyMark(actions.currencyId)}',
                color: actions.expense
                    ? AppColors.RED_TEXT_LISTVIEW
                    : AppColors.GREEN,
                fontSize: 15,
                fontWeight: FontWeight.w600,
              )
            ],
          ),
        ),
      ),
    );
  }

  String _getCurrencyMark(int current) {
    if (listCurrency.isNotEmpty) {
      Currency object = listCurrency[current];
      if (SharedPrefController().getValueLang == 'en') {
        return object.nameEn;
      } else {
        return object.nameAr;
      }
    }
    return '';
  }
}
