import 'package:budget_planner_app/utils/app_colors.dart';
import 'package:budget_planner_app/utils/size_config.dart';
import 'package:flutter/material.dart';
import 'my_text.dart';

class ContainerSettings extends StatelessWidget {
  String title;
  IconData? iconDataLeading, iconDataTralling;
  GestureTapCallback onPreased;
  bool visible;
  bool border;
  Color textColor;
  Color iconColor;
  Color bgColor;

  ContainerSettings({
    required this.title,
    required this.iconDataLeading,
    required this.onPreased,
    this.iconDataTralling,
    this.visible = true,
    this.border = true,
    this.textColor = AppColors.TEXT_BOLD,
    this.iconColor = AppColors.TEXT_BOLD,
    this.bgColor = AppColors.WHITE,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      height: SizeConfig.scaleHeight(70),
      decoration: BoxDecoration(
        color: bgColor,
        borderRadius: BorderRadius.circular(8),
        border: border
            ? null
            : Border.all(color: AppColors.RED_Button_DELETE, width: 1),
        boxShadow: [
          BoxShadow(
            color: AppColors.SHADOW2,
            blurRadius: 7,
            offset: Offset(0, 3),
          ),
        ],
      ),
      child: Material(
        color: bgColor,
        borderRadius: BorderRadius.circular(8),
        child: InkWell(
          onTap: onPreased,
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              SizedBox(width: SizeConfig.scaleWidth(20)),
              Icon(iconDataLeading, color: iconColor, size: 24),
              SizedBox(width: SizeConfig.scaleWidth(10)),
              Expanded(
                child: MyText(
                  title: title,
                  fontSize: 15,
                  textAlign: TextAlign.start,
                  fontWeight: FontWeight.w500,
                  color: textColor,
                ),
              ),
              Visibility(
                visible: visible,
                child: Icon(
                  iconDataTralling,
                  color: textColor,
                  size: 14,
                ),
              ),
              SizedBox(width: SizeConfig.scaleWidth(13)),
            ],
          ),
        ),
      ),
    );
  }
}
