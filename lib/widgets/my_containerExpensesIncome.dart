import 'package:budget_planner_app/utils/app_colors.dart';
import 'package:budget_planner_app/utils/size_config.dart';
import 'package:flutter/material.dart';

import 'my_text.dart';

class MyContianerExpensesIncome extends StatelessWidget {
  String icon;
  String title;
  GestureTapCallback? onPressed;
  Color borderColor;

  MyContianerExpensesIncome({
    required this.icon,
    required this.title,
    this.onPressed,
    this.borderColor=AppColors.WHITE,
  });

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: Container(
        alignment: Alignment.center,
        height: SizeConfig.scaleHeight(92),
        decoration: BoxDecoration(
          color: AppColors.WHITE,
          borderRadius: BorderRadius.circular(8),
          border: Border.all(
            color: borderColor,
            width: 2,
          ),
          boxShadow: [
            BoxShadow(
              color: AppColors.BLACK.withOpacity(.16),
              blurRadius: 6,
              offset: Offset(0, 3),
            ),
          ],
        ),
        child: Material(
          color: AppColors.WHITE,
          borderRadius: BorderRadius.circular(8),
          child: InkWell(
            borderRadius: BorderRadius.circular(8),
            onTap: onPressed,
            child: Container(
              width: double.infinity,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Image.asset(
                    icon,
                    fit: BoxFit.fill,
                    width: SizeConfig.scaleWidth(16),
                    height: SizeConfig.scaleHeight(20),
                  ),
                  SizedBox(height: SizeConfig.scaleHeight(4)),
                  MyText(
                    title: title,
                    fontSize: 15,
                    fontWeight: FontWeight.w500,
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
