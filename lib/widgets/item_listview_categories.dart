import 'package:budget_planner_app/utils/app_colors.dart';
import 'package:budget_planner_app/utils/size_config.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'my_text.dart';

class ItemListViewCategories extends StatelessWidget {
  String title;
  int countAction;

  ItemListViewCategories({
    required this.title,
    required this.countAction,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      height: SizeConfig.scaleHeight(60),
      margin: EdgeInsetsDirectional.only(
        start: SizeConfig.scaleWidth(15),
        end: SizeConfig.scaleWidth(15),
      ),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            width: SizeConfig.scaleWidth(54),
            height: SizeConfig.scaleHeight(54),
            margin: EdgeInsetsDirectional.only(
              end: SizeConfig.scaleWidth(13),
            ),
            alignment: Alignment.center,
            decoration: BoxDecoration(
              color: AppColors.BG_PROGRESS,
              borderRadius: BorderRadius.circular(16),
            ),
            child: MyText(
              title: title.substring(0, 1).toUpperCase(),
            ),
          ),
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                SizedBox(height: SizeConfig.scaleHeight(4)),
                MyText(
                  title: title,
                  fontSize: 15,
                  fontWeight: FontWeight.w500,
                ),
                MyText(
                  title: '${countAction.toString()} ${AppLocalizations.of(context)!.action}',
                  fontSize: 11,
                  fontWeight: FontWeight.w500,
                  color: AppColors.HINT_TEXT,
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
