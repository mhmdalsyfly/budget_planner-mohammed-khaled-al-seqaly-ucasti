import 'package:budget_planner_app/utils/app_colors.dart';
import 'package:budget_planner_app/utils/size_config.dart';
import 'package:flutter/material.dart';

import 'my_text.dart';

class ItemCurrency extends StatefulWidget {
  String image;
  String title;
  GestureTapCallback? onTap;
  bool enabled;

  ItemCurrency({
    required this.image,
    required this.title,
    required this.enabled,
    this.onTap,
  });

  @override
  _ItemCurrencyState createState() => _ItemCurrencyState();
}

class _ItemCurrencyState extends State<ItemCurrency> {
  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: SizeConfig.scaleHeight(65),
      width: double.infinity,
      child: Material(
        color: AppColors.WHITE,
        child: InkWell(
          onTap: widget.onTap,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Image.asset(
                    widget.image, //'images/dollar.png'
                    fit: BoxFit.fill,
                    width: SizeConfig.scaleWidth(28),
                    height: SizeConfig.scaleHeight(28),
                  ),
                  SizedBox(width: SizeConfig.scaleWidth(8)),
                  Expanded(
                    child: MyText(
                      title: widget.title,
                      //United States Dollar (USD \$)
                      fontSize: 15,
                      textAlign: TextAlign.start,
                      fontWeight: FontWeight.w500,
                      color: AppColors.Label_TextFiled,
                    ),
                  ),
                  Visibility(
                    visible: widget.enabled,
                    child: Icon(Icons.check, color: AppColors.BG_BUTTON),
                  ),
                ],
              ),
              SizedBox(height: SizeConfig.scaleHeight(15)),
              Divider(height: 0),
            ],
          ),
        ),
      ),
    );
  }
}
