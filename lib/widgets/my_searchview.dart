import 'package:budget_planner_app/utils/app_colors.dart';
import 'package:budget_planner_app/utils/size_config.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class MySearchView extends StatelessWidget {
  TextEditingController searchViewController;
  Color bg_search;
  ValueChanged<String>? onChange;

  MySearchView({
    required this.searchViewController,
    this.bg_search = AppColors.WHITE,
    this.onChange,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      clipBehavior: Clip.antiAlias,
      height: SizeConfig.scaleHeight(55),
      decoration: BoxDecoration(
        color: bg_search,
        borderRadius: BorderRadius.circular(8),
      ),
      child: Row(
        children: [
          IconButton(
            icon: Icon(Icons.search, color: AppColors.TEXT_BOLD),
            onPressed: () {},
          ),
          Expanded(
            child: TextField(
              textAlign: TextAlign.start,
              controller: searchViewController,
              onChanged: onChange,
              decoration: InputDecoration(
                focusedBorder: InputBorder.none,
                enabledBorder: InputBorder.none,
                hintText: AppLocalizations.of(context)!.search,
                hintStyle: TextStyle(
                  color: AppColors.HINT_TEXT,
                  fontFamily: 'Montserrat',
                  fontWeight: FontWeight.w400,
                  fontSize: SizeConfig.scaleTextFont(15),
                ),
              ),
            ),
          )
        ],
      ),
    );
  }
}
