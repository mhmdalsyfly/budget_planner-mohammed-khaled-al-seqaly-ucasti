import 'package:budget_planner_app/utils/app_colors.dart';
import 'package:budget_planner_app/utils/size_config.dart';
import 'package:flutter/material.dart';

class MyTextField1 extends StatelessWidget {
  TextInputType textInputType;
  TextEditingController controller;
  String textHint;
  double? height;
  double xOffset;
  double hOffset;
  double blur;
  double paddingStart;
  double paddingTop;
  double paddingEnd;
  double paddingBottom;
  double radius;
  Color shadow;
  AlignmentDirectional alignment;
  bool enabled;
  ValueChanged<String>? onChange;

  MyTextField1({
    this.textInputType = TextInputType.text,
    required this.controller,
    required this.textHint,
    this.height,
    this.hOffset = 0,
    this.xOffset = 0,
    this.blur = 5,
    this.paddingStart = 0,
    this.paddingTop = 0,
    this.paddingEnd = 0,
    this.paddingBottom = 0,
    this.alignment = AlignmentDirectional.center,
    this.enabled = true,
    this.onChange,
    this.radius = 10,
    this.shadow = AppColors.SHADOW3,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      height: height,
      alignment: alignment,
      padding: EdgeInsetsDirectional.only(
        end: SizeConfig.scaleWidth(paddingEnd),
        start: SizeConfig.scaleWidth(paddingStart),
        top: SizeConfig.scaleHeight(paddingTop),
        bottom: SizeConfig.scaleHeight(paddingBottom),
      ),
      decoration: BoxDecoration(
        color: AppColors.BG_SCREEN_COLOR,
        borderRadius: BorderRadius.circular(radius),
        boxShadow: [
          BoxShadow(
            offset: Offset(xOffset, hOffset),
            color: shadow,
            blurRadius: blur,
          ),
        ],
      ),
      child: TextField(
        maxLines: 4,
        minLines: 1,
        keyboardType: textInputType,
        controller: controller,
        enabled: enabled,
        onChanged: onChange,
        cursorColor: AppColors.GRAY,
        style: TextStyle(
          fontSize: 15,
          fontWeight: FontWeight.w400,
          fontFamily: 'Montserrat',
          color: AppColors.BLACK.withOpacity(.8),
        ),
        decoration: InputDecoration(
          contentPadding: EdgeInsets.symmetric(
            horizontal: SizeConfig.scaleWidth(20),
            vertical: SizeConfig.scaleHeight(10),
          ),
          hintText: textHint,
          hintStyle: TextStyle(
            fontSize: 15,
            fontWeight: FontWeight.w400,
            fontFamily: 'Montserrat',
            color: AppColors.GRAY,
          ),
          enabledBorder: InputBorder.none,
          focusedBorder: InputBorder.none,
          disabledBorder: InputBorder.none,
        ),
      ),
    );
  }
}
