import 'package:budget_planner_app/utils/app_colors.dart';
import 'package:budget_planner_app/utils/size_config.dart';
import 'package:flutter/material.dart';

class MyText extends StatelessWidget {
  String title;
  Color color;
  double fontSize;
  double height;
  FontWeight fontWeight;
  TextAlign textAlign;

  MyText({
    required this.title,
    this.color = AppColors.TEXT_BOLD,
    this.fontSize = 20,
    this.height = 1.3,
    this.fontWeight = FontWeight.bold,
    this.textAlign = TextAlign.center,
  });

  @override
  Widget build(BuildContext context) {
    return Text(
      title,
      textAlign: textAlign,
      style: TextStyle(
        color: color,
        fontSize: SizeConfig.scaleTextFont(fontSize),
        fontWeight: fontWeight,
        height: height,
        fontFamily: 'Montserrat',
      ),
    );
  }
}
