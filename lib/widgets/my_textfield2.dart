import 'package:budget_planner_app/utils/app_colors.dart';
import 'package:budget_planner_app/utils/size_config.dart';
import 'package:flutter/material.dart';

import 'my_text.dart';

class MyTextField2 extends StatelessWidget {
  TextInputType textInputType;
  TextEditingController controller;
  String title;
  String subTitle;
  bool check;
  GestureTapCallback? onTap;
  bool enabled;
  ValueChanged<String>? onChange;

  MyTextField2({
    this.textInputType = TextInputType.text,
    required this.controller,
    required this.title,
    this.subTitle = '',
    this.check = false,
    this.enabled = true,
    this.onTap,
    this.onChange,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      height: SizeConfig.scaleHeight(70),
      child: Column(
        children: [
          Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              MyText(
                title: title,
                fontSize: 15,
                color: AppColors.Label_TextFiled,
                fontWeight: FontWeight.w500,
              ),
              Expanded(
                child: GestureDetector(
                  onTap: onTap,
                  child: TextField(
                    keyboardType: textInputType,
                    enabled: enabled,
                    controller: controller,
                    onChanged: onChange,
                    style: TextStyle(
                      color: AppColors.GRAY,
                      fontSize: 15,
                      fontFamily: 'Montserrat',
                      fontWeight: FontWeight.w400,
                    ),
                    cursorColor: AppColors.GRAY,
                    textAlign: TextAlign.end,
                    decoration: InputDecoration(
                      hintText: subTitle,
                      enabledBorder: InputBorder.none,
                      focusedBorder: InputBorder.none,
                    ),
                  ),
                ),
              ),
              Visibility(
                visible: check,
                child: Material(
                  child: InkWell(
                    onTap: onTap,
                    child: Icon(
                      Icons.arrow_forward_ios,
                      color: Color(0xff555568),
                      size: 14,
                    ),
                  ),
                ),
              ),
            ],
          ),
          Divider(
            height: 0,
            color: AppColors.GRAY,
          ),
        ],
      ),
    );
  }
}
