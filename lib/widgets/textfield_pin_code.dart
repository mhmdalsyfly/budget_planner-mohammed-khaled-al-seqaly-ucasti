import 'package:budget_planner_app/utils/app_colors.dart';
import 'package:budget_planner_app/utils/size_config.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class TextFieldPinCode extends StatefulWidget {
  final TextEditingController firstCodeTextEditingController;
  final TextEditingController? secondCodeTextEditingController;
  final FocusNode firstCodeFocusNode;
  final FocusNode? secondCodeFocusNode;
  final bool check;

  TextFieldPinCode({
    required this.firstCodeTextEditingController,
    this.secondCodeTextEditingController,
    required this.firstCodeFocusNode,
    this.secondCodeFocusNode,
    this.check = true,
  });

  @override
  _TextFieldPinCodeState createState() => _TextFieldPinCodeState();
}

class _TextFieldPinCodeState extends State<TextFieldPinCode> {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: SizeConfig.scaleHeight(45),
      width: SizeConfig.scaleWidth(45),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(8),
        boxShadow: [
          BoxShadow(
            color: AppColors.BLACK.withOpacity(.16),
            blurRadius: 6,
            offset: Offset(0, 0),
          ),
        ],
      ),
      child: TextField(
        controller: widget.firstCodeTextEditingController,
        focusNode: widget.firstCodeFocusNode,
        showCursor: false,
        minLines: null,
        maxLines: null,
        expands: true,
        keyboardType: TextInputType.number,
        maxLength: 1,
        style: TextStyle(
          fontSize: SizeConfig.scaleTextFont(23),
          color: AppColors.WHITE,
          fontWeight: FontWeight.bold,
        ),
        textAlign: TextAlign.center,
        onChanged: (String text) {
          if(widget.check)
          if (text.length == 1) {
            if(widget.secondCodeTextEditingController!.text.toString().isNotEmpty){
              setState(() {
                widget.secondCodeTextEditingController!.text='';
              });
            }
            widget.secondCodeFocusNode!.requestFocus();
          }
        },
        decoration: InputDecoration(
          counterText: '',
          fillColor: getColor(),
          filled: true,
          contentPadding: EdgeInsets.zero,
          enabledBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(8),
            borderSide: BorderSide.none,
          ),
          focusedBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(8),
            borderSide: BorderSide.none,
          ),
        ),
      ),
    );
  }

  Color getColor() {
    if (widget.firstCodeTextEditingController.text.isNotEmpty)
      return AppColors.BG_BUTTON;
    return AppColors.WHITE;
  }
}
