import 'package:budget_planner_app/model/tips.dart';
import 'package:budget_planner_app/screens/details_tips.dart';
import 'package:budget_planner_app/utils/app_colors.dart';
import 'package:budget_planner_app/utils/size_config.dart';
import 'package:flutter/material.dart';
import 'my_contianer_logo.dart';
import 'my_text.dart';

class ItemTips extends StatelessWidget {
  final Tipss tips;
  int index;

  ItemTips(this.tips, this.index);

  @override
  Widget build(BuildContext context) {
    return MyContianerLogo(
      enableShadow: false,
      borderRadius: 8,
      paddingVertical: 13,
      paddingHorizontal: 13,
      child: GestureDetector(
        onTap: () {
          Navigator.push(
            context,
            MaterialPageRoute(
              builder: (context) => DetailsTipsScreen(tips),
            ),
          );
        },
        child: Column(
          children: [
            Expanded(
              child: Image.asset(
                tips.image,
                fit: BoxFit.fill,
              ),
            ),
            SizedBox(height: SizeConfig.scaleHeight(8)),
            MyText(
              title: tips.title,
              color: AppColors.TEXT_BOLD,
              fontSize: 18,
              height: 1.4,
              fontWeight: FontWeight.w600,
            ),
            SizedBox(height: SizeConfig.scaleHeight(6)),
            MyText(
              title: tips.date,
              color: AppColors.TEXT_NOTE,
              fontSize: 13,
              height: 1,
              fontWeight: FontWeight.w400,
            ),
          ],
        ),
      ),
    );
  }
}
