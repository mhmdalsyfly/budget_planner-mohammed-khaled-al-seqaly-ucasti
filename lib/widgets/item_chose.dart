import 'package:budget_planner_app/utils/app_colors.dart';
import 'package:budget_planner_app/utils/size_config.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'my_text.dart';

class ItemChose extends StatelessWidget {
  String title;
  GestureTapCallback onTap;
  String hintTitle;

  ItemChose({
    required this.title,
    required this.onTap,
    required this.hintTitle,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      height: SizeConfig.scaleHeight(50),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          MyText(
            title: title,
            fontSize: 15,
            fontWeight: FontWeight.w500,
            color: AppColors.Label_TextFiled,
          ),
          Spacer(),
          GestureDetector(
            onTap: onTap,
            child: MyText(
              title: hintTitle,
              fontSize: 15,
              fontWeight: FontWeight.w400,
              color: AppColors.GRAY,
            ),
          ),
          GestureDetector(
            onTap: onTap,
            child: Icon(
              Icons.arrow_forward_ios_outlined,
              size: 14,
            ),
          ),
        ],
      ),
    );
  }
}
