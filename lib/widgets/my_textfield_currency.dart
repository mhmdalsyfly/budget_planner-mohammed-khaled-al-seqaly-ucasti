import 'package:budget_planner_app/utils/app_colors.dart';
import 'package:budget_planner_app/utils/size_config.dart';
import 'package:flutter/material.dart';

class MyTextFieldCurrency extends StatelessWidget {

  TextEditingController controller;
  ValueChanged<String>? onChange;

  MyTextFieldCurrency({required this.controller,this.onChange});

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: SizeConfig.scaleWidth(100),
      height: SizeConfig.scaleHeight(40),
      child: TextField(
        onChanged: onChange,
        keyboardType: TextInputType.number,
        controller: controller,
        textAlign: TextAlign.center,
        showCursor: false,
        style: TextStyle(
          color: AppColors.Label_TextFiled,
          fontSize: 21,
          fontWeight: FontWeight.w600,
        ),
        decoration: InputDecoration(
          hintText: '0 \$\$',
          enabledBorder: InputBorder.none,
          focusedBorder: UnderlineInputBorder(),
        ),
      ),
    );
  }
}

