import 'package:budget_planner_app/utils/app_colors.dart';
import 'package:flutter/material.dart';

import 'my_text.dart';

class MyTextButton2 extends StatelessWidget {
  final BuildContext context;
  final String title;
  final VoidCallback onPreese;
  final bool colorYes;

  MyTextButton2({
    required this.context,
    required this.title,
    required this.onPreese,
    this.colorYes = false,
  });

  @override
  Widget build(BuildContext context) {
    return TextButton(
      onPressed: onPreese,
      child: MyText(
        title: title,
        fontWeight: FontWeight.w500,
        fontSize: 12,
        color: colorYes? AppColors.RED_Button_DELETE : AppColors.TEXT_BOLD,
      ),
    );
  }
}
