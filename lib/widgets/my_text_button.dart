import 'package:budget_planner_app/utils/app_colors.dart';
import 'package:budget_planner_app/utils/size_config.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'my_text.dart';

class MyTextButton extends StatelessWidget {
  String title;
  VoidCallback onPreesed;
  bool check;
  Color textColor;
  Color checkBgColor;

  MyTextButton({
    required this.title,
    required this.onPreesed,
    this.check = true,
    this.textColor = AppColors.WHITE,
    this.checkBgColor = AppColors.BG_BUTTON,
  });

  @override
  Widget build(BuildContext context) {
    return TextButton(
      onPressed: onPreesed,
      style: check
          ? TextButton.styleFrom(
              backgroundColor: checkBgColor,
              minimumSize: Size(
                double.infinity,
                SizeConfig.scaleHeight(60),
              ),
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(40),
              ),
            )
          : null,
      child: MyText(
        title: title,
        color: textColor,
        fontSize: 15,
        fontWeight: check ? FontWeight.bold : FontWeight.w500,
      ),
    );
  }
}
