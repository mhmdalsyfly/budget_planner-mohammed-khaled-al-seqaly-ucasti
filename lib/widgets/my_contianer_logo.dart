import 'package:budget_planner_app/utils/app_colors.dart';
import 'package:budget_planner_app/utils/size_config.dart';
import 'package:flutter/material.dart';

class MyContianerLogo extends StatelessWidget {
  Widget? child;
  Color shadow;
  Color bg_Container;
  bool enableShadow;
  bool enableRadius;
  BoxShape boxShape;
  AlignmentDirectional? alignment;
  double? height;
  double width;
  double paddingVertical;
  double paddingHorizontal;
  double borderRadius;
  double xOffset;
  double yOffset;
  double blur;
  double marginTop;
  double marginBottom;
  double marginStart;
  double marginEnd;

  MyContianerLogo({
    this.height,
    this.width = 120,
    this.boxShape = BoxShape.rectangle,
    this.paddingVertical = 30,
    this.paddingHorizontal = 30,
    this.marginTop = 0,
    this.marginBottom = 0,
    this.marginStart = 0,
    this.marginEnd = 0,
    this.borderRadius = 30,
    this.xOffset = 0,
    this.yOffset = 10,
    this.blur = 18,
    this.shadow = AppColors.SHADOW,
    this.alignment,
    this.enableShadow = true,
    this.enableRadius = true,
    this.bg_Container = AppColors.BG_SCREEN_COLOR,
    this.child,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      width: SizeConfig.scaleWidth(width),
      clipBehavior: Clip.antiAlias,
      height: height,
      alignment: alignment,
      padding: EdgeInsets.symmetric(
        vertical: SizeConfig.scaleHeight(paddingVertical),
        horizontal: SizeConfig.scaleWidth(paddingHorizontal),
      ),
      margin: EdgeInsetsDirectional.only(
        top: SizeConfig.scaleHeight(marginTop),
        bottom: SizeConfig.scaleHeight(marginBottom),
        start: SizeConfig.scaleWidth(marginStart),
        end: SizeConfig.scaleWidth(marginEnd),
      ),
      decoration: BoxDecoration(
        color: bg_Container,
        shape: boxShape,
        borderRadius: enableRadius ? BorderRadius.circular(borderRadius) : null,
        boxShadow: enableShadow
            ? [
                BoxShadow(
                  color: shadow,
                  offset: Offset(xOffset, yOffset),
                  blurRadius: blur,
                ),
              ]
            : null,
      ),
      child: child,
    );
  }
}
