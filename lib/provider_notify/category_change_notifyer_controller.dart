import 'package:budget_planner_app/model/category.dart';
import 'package:budget_planner_app/storage/controller/category_controller.dart';
import 'package:flutter/cupertino.dart';

class CategoryChangeNotifierController extends ChangeNotifier {
  List<Category> listCategory = [];
  List<Category> listCategoryExpenses = [];
  List<Category> listCategoryIncome = [];
  late CategoryController categoryController;

  CategoryChangeNotifierController() {
    categoryController = CategoryController();
  }

  Future<int> create(Category category) async {
    int value = await categoryController.create(category);
    if (value > 0) {
      category.id = value;
      listCategory.add(category);
      if (category.expense == 1)
        listCategoryExpenses.add(category);
      else
        listCategoryIncome.add(category);
      notifyListeners();
    }
    return value;
  }

  Future delete(int id) async {
    bool value = await categoryController.delete(id);
    if (value) {
      listCategory.removeWhere((element) => element.id == id);
      notifyListeners();
    }
  }
  Future clearDataUser() async {
    bool value = await categoryController.clearDataUser();
    if (value) {
      listCategory.clear();
      listCategoryExpenses.clear();
      listCategoryIncome.clear();
      notifyListeners();
    }
  }

  Future readCategoryExpenses() async {
    listCategoryExpenses = await categoryController.readCategoryExpenses();
    notifyListeners();
  }

  Future readCategoryIncome() async {
    listCategoryIncome = await categoryController.readCategoryIncome();
    notifyListeners();
  }

  Future<bool> update(Category category) async {
    bool value = await categoryController.update(category);
    if (value) {
      int index =
          listCategory.indexWhere((element) => element.id == category.id);
      listCategory[index] = category;
      notifyListeners();
    }
    return value;
  }



}
