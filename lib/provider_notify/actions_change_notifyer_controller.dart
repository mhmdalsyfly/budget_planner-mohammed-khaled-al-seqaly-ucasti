import 'package:budget_planner_app/model/category_actions.dart';
import 'package:budget_planner_app/model/user.dart';
import 'package:budget_planner_app/storage/controller/actions_controller.dart';
import 'package:budget_planner_app/storage/pref/shared_pref_controller.dart';
import 'package:flutter/cupertino.dart';
import 'package:intl/intl.dart';

class ActionsChangeNotifierController extends ChangeNotifier {
  List<CategoryActions> listAction = [];
  List<CategoryActions> listLastAction = [];
  List<CategoryActions> listActionSearch = [];

  late ActionController actionController;

  String currentDay = '';
  late int wallet;
  int wallet2 = 0;

  ActionsChangeNotifierController() {
    actionController = ActionController();
    wallet = SharedPrefController().getUser().dayLimit;
  }

  Future<int> create(CategoryActions action) async {
    int value = await actionController.create(action);
    if (value > 0) {
      action.id = value;
      listAction.insert(0, action);
      listLastAction.insert(0, action);
      listActionSearch.insert(0, action);
      notifyListeners();
    }
    return value;
  }

  Future read() async {
    wallet2 = 0;
    wallet = SharedPrefController().getUser().dayLimit;
    listAction = await actionController.read();
    listActionSearch = await actionController.read();
    Future.delayed(Duration.zero, () {
      getAmountDay();
    });
    notifyListeners();
  }

  Future readLastActions() async {
    listLastAction = await actionController.readLastActions();
    var date = DateTime.now();
    currentDay = DateFormat('yyyy-MM-dd').format(date); // prints 2021-07-03
    notifyListeners();
  }

  void search(String char) {
    listActionSearch.clear();
    listAction.forEach((CategoryActions object) {
      if (object.category.toLowerCase().contains(char.toLowerCase())) {
        listActionSearch.add(object);
      }
    });
    notifyListeners();
  }

  getAmountDay() {
    for (int i = 0; i < listAction.length; i++) {
      if (currentDay == listAction[i].date) {
        setAmount(
          expense: listAction[i].expense,
          amount: listAction[i].amount.toDouble(),
          idCurrency: listAction[i].currencyId,
        );
        notifyListeners();
      }
    }
  }

  setAmount({
    required double amount,
    required int idCurrency,
    required bool expense,
  }) {
    double conversion = amount;
    int currencyUser = SharedPrefController().getUser().currencyId;

    if (idCurrency == 0 && currencyUser == idCurrency) {
      // العملة اللي معي واللي اجتني شيكل
      conversion = amount;
      changeOnWallet(expense: expense, conversion: conversion.toInt());
    } else if (idCurrency == 1 && currencyUser == idCurrency) {
      // العملة اللي معي واللي اجتني دولار
      conversion = amount;
      changeOnWallet(expense: expense, conversion: conversion.toInt());
    } else if (idCurrency == 2 && currencyUser == idCurrency) {
      // العملة اللي معي واللي اجتني دينار
      conversion = amount;
      changeOnWallet(expense: expense, conversion: conversion.toInt());
    } else if (currencyUser == 0 && idCurrency == 1) {
      // العملة اللي اجتني دولار ونا معي شيكل
      conversion = amount * 3.27;
      changeOnWallet(expense: expense, conversion: conversion.toInt());
    } else if (currencyUser == 0 && idCurrency == 2) {
      // العلمة اللي اجتني دينار ونا معي شيكل
      conversion = amount * 4.62;
      changeOnWallet(expense: expense, conversion: conversion.toInt());
    } else if (currencyUser == 1 && idCurrency == 0) {
      // العملة اللي اجتني شيكل ونا معي دولار
      conversion = amount / 3.27;
      changeOnWallet(expense: expense, conversion: conversion.toInt());
    } else if (currencyUser == 1 && idCurrency == 2) {
      // العلمة اللي اجتني دينار ونا معي دولار
      conversion = amount * 1.41;
      changeOnWallet(expense: expense, conversion: conversion.toInt());
    } else if (currencyUser == 2 && idCurrency == 0) {
      // العملة اللي اجتني شيكل ونا معي دينار
      conversion = amount / 4.62;
      changeOnWallet(expense: expense, conversion: conversion.toInt());
    } else if (currencyUser == 2 && idCurrency == 1) {
      // العلمة اللي احتني دولار ونا معي دينار
      conversion = amount / 1.41;
      changeOnWallet(expense: expense, conversion: conversion.toInt());
    }
    notifyListeners();
  }

  changeOnWallet({required bool expense, required int conversion}) {
    if (expense) {
      wallet = wallet - conversion;
      wallet2 += conversion;
    } else {
      wallet = wallet + conversion;
    }
  }

  clearDataAction() {
    listAction.clear();
    listLastAction.clear();
    listActionSearch.clear();
    wallet = SharedPrefController().getUser().dayLimit;
    wallet2 = 0;
    notifyListeners();
  }
}
