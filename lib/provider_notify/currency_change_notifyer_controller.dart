import 'package:budget_planner_app/model/currency.dart';
import 'package:budget_planner_app/storage/controller/currency_controller.dart';
import 'package:flutter/cupertino.dart';

class CurrencyChangeNotifierController extends ChangeNotifier {
  late CurrencyController currencyController;
  List<Currency> listCurrency = [];

  CurrencyChangeNotifierController() {
    currencyController = CurrencyController();
    readCurrency();
  }

  Future<void> readCurrency() async {
    if(listCurrency.isEmpty){
      listCurrency = await currencyController.read();
      notifyListeners();
    }

  }
}
