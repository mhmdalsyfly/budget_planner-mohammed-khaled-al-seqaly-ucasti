import 'package:budget_planner_app/model/user.dart';
import 'package:budget_planner_app/storage/pref/shared_pref_controller.dart';
import 'package:budget_planner_app/storage/controller/user_controller.dart';
import 'package:flutter/cupertino.dart';

class UserChangeNotifierController extends ChangeNotifier {
  List<User> listUsers = [];
  late UserController userController;
  User user = SharedPrefController().getUser();

  UserChangeNotifierController() {
    userController = UserController();
    read();
  }

  Future<int> create(User user) async {
    int value = await userController.create(user);
    if (value > 0) {
      user.id = value;
      SharedPrefController().setUser(user);
      listUsers.add(user);
      notifyListeners();
    }
    return value;
  }

  Future delete(int id) async {
    bool value = await userController.delete(id);
    if (value) {
      SharedPrefController().setUser(
        User(
          id: -1,
          name: '',
          email: '',
          currencyId: -1,
          dayLimit: -1,
          pin: -1,
        ),
      );
      listUsers.removeWhere((element) => element.id == id);
      notifyListeners();
    }
  }

  Future read() async {
    listUsers = await userController.read();
    notifyListeners();
  }

  Future<bool> update(User user) async {
    bool value = await userController.update(user);
    if (value) {
      SharedPrefController().setUser(user);
      setUser(user);
      int index = listUsers.indexWhere((element) => element.id == user.id);
      listUsers[index] = user;
      notifyListeners();
    }
    return value;
  }

  User getUser() {
    return user;
  }

  setUser(User user){
    this.user = user;
    notifyListeners();
  }

  Future<bool> clearActionUser() async {
    return await userController.clearDataActions();
  }
}
