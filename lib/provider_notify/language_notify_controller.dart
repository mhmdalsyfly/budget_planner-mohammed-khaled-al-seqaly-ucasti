import 'package:flutter/material.dart';

class LanguageNotifyController extends ChangeNotifier{
  Locale _locale;

  LanguageNotifyController(this._locale);

  Locale get locale => this._locale;

  void changeLanguage(Locale locale) {
    this._locale = locale;
    notifyListeners();
  }
}
