import 'package:budget_planner_app/bottom_nav/main_bottom_navigation_screen.dart';
import 'package:budget_planner_app/on_baording/page_view_screen.dart';
import 'package:budget_planner_app/provider_notify/actions_change_notifyer_controller.dart';
import 'package:budget_planner_app/provider_notify/category_change_notifyer_controller.dart';
import 'package:budget_planner_app/provider_notify/currency_change_notifyer_controller.dart';
import 'package:budget_planner_app/provider_notify/language_notify_controller.dart';
import 'package:budget_planner_app/provider_notify/user_change_notifyer_controller.dart';
import 'package:budget_planner_app/screens/actions_screen.dart';
import 'package:budget_planner_app/screens/add_category_screen.dart';
import 'package:budget_planner_app/screens/add_operation_screen.dart';
import 'package:budget_planner_app/screens/choose_category_screen.dart';
import 'package:budget_planner_app/screens/choose_currency_screen.dart';
import 'package:budget_planner_app/screens/create_account_screen.dart';
import 'package:budget_planner_app/screens/created_account_screen.dart';
import 'package:budget_planner_app/screens/info_app_screen.dart';
import 'package:budget_planner_app/screens/language_screen.dart';
import 'package:budget_planner_app/screens/launch_screen.dart';
import 'package:budget_planner_app/screens/login_screen.dart';
import 'package:budget_planner_app/screens/new_operation_success.dart';
import 'package:budget_planner_app/screens/pin_code_screen.dart';
import 'package:budget_planner_app/screens/setting_screen.dart';
import 'package:budget_planner_app/storage/db/db_provider.dart';
import 'package:budget_planner_app/storage/pref/shared_pref_controller.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:provider/provider.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await SharedPrefController().initSharedPref();
  await DBProvider().initDatabase();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider<CurrencyChangeNotifierController>(
            create: (context) => CurrencyChangeNotifierController()),
        ChangeNotifierProvider<UserChangeNotifierController>(
            create: (context) => UserChangeNotifierController()),
        ChangeNotifierProvider<CategoryChangeNotifierController>(
            create: (context) => CategoryChangeNotifierController()),
        ChangeNotifierProvider<ActionsChangeNotifierController>(
            create: (context) => ActionsChangeNotifierController()),
        ChangeNotifierProvider<LanguageNotifyController>(
          create: (context) => LanguageNotifyController(
            Locale(SharedPrefController().getValueLang),
          ),
        ),
      ],
      child: Consumer<LanguageNotifyController>(
        builder: (
          BuildContext context,
          LanguageNotifyController value,
          Widget? child,
        ) {
          return MaterialApp(
            localizationsDelegates: AppLocalizations.localizationsDelegates,
            supportedLocales: AppLocalizations.supportedLocales,
            debugShowCheckedModeBanner: false,
            locale: value.locale,
            initialRoute: '/launch_screen',
            routes: {
              '/launch_screen': (context) => LaunchScreen(),
              '/page_view_screen': (context) => PageViewScreen(),
              '/login_screen': (context) => LoginScreen(),
              '/create_account_screen': (context) => CreateAccountScreen(),
              '/created_account_screen': (context) => CreatedAccountScreen(),
              '/pin_code_screen': (context) => PinCodeScreen(),
              '/main_bottom_navigation_screen': (context) =>
                  MainBottomNavigationScreen(),
              '/settings_screen': (context) => SettingsScreen(),
              '/info_app_screen': (context) => InfoAppScreen(),
              '/actions_screen': (context) => ActionsScreen(),
              '/add_category_screen': (context) => AddCategoryScreen(),
              '/add_operation_screen': (context) => AddOperationScreen(),
              '/chose_currency_screen': (context) => ChoseCurrencyScreen(),
              '/chose_category_screen': (context) => ChooseCategoryScreen(),
              '/new_operation_success': (context) => NewOperationSuccess(),
              '/language_screen': (context) => LanguageScreen(),
            },
          );
        },
      ),
    );
  }
}
/*

* */
