import 'package:budget_planner_app/model/category.dart';
import 'package:budget_planner_app/provider_notify/category_change_notifyer_controller.dart';
import 'package:budget_planner_app/utils/app_colors.dart';
import 'package:budget_planner_app/utils/size_config.dart';
import 'package:budget_planner_app/widgets/item_listview_categories.dart';
import 'package:budget_planner_app/widgets/my_contianer_logo.dart';
import 'package:budget_planner_app/widgets/my_text.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class ExpensesIncome extends StatefulWidget {
  String title;

  ExpensesIncome({required this.title});

  @override
  _ExpensesIncomeState createState() => _ExpensesIncomeState();
}

class _ExpensesIncomeState extends State<ExpensesIncome> {
  List<Category> listCategories = [];

  @override
  Widget build(BuildContext context) {
    return MyContianerLogo(
      width: double.infinity,
      marginBottom: 15,
      bg_Container: AppColors.WHITE,
      borderRadius: 8,
      blur: 18,
      xOffset: 0,
      yOffset: 10,
      paddingHorizontal: 0,
      paddingVertical: 0,
      child: Consumer<CategoryChangeNotifierController>(
        builder: (
          BuildContext context,
          CategoryChangeNotifierController value,
          Widget? child,
        ) {
          widget.title == AppLocalizations.of(context)!.expenses
              ? listCategories = value.listCategoryExpenses
              : listCategories = value.listCategoryIncome;
          return listCategories.length == 0 ? Center(
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                Icon(Icons.error_outline,size: 80,color: AppColors.BG_BUTTON,),
                SizedBox(height: SizeConfig.scaleHeight(16)),
                MyText(title: AppLocalizations.of(context)!.noData),
              ],
            ),
          ): ListView.separated(
            itemBuilder: (context, index) => ItemListViewCategories(
              title: listCategories[index].name,
              countAction: listCategories[index].countAction ,
            ),
            padding: EdgeInsets.only(
              top: SizeConfig.scaleHeight(10),
              bottom: SizeConfig.scaleHeight(10),
            ),
            separatorBuilder: (context, index) => Column(
              children: [
                Padding(
                  padding: EdgeInsets.symmetric(
                    horizontal: SizeConfig.scaleWidth(15),
                  ),
                  child: Divider(),
                ),
                SizedBox(
                  height: SizeConfig.scaleHeight(5),
                ),
              ],
            ),
            itemCount: listCategories.length,
          );
        },
      ),
    );
  }
}