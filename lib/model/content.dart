class Content {
  final String title;
  final String subTitle;
  final String image;

  Content({
    required this.title,
    required this.subTitle,
    required this.image,
  });
}
