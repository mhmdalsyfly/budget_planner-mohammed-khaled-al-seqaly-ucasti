import 'package:budget_planner_app/model/db_table.dart';

class CategoryActions {
  int? id = 0;
  late int amount;
  late String date;
  late bool expense;
  late String category;
  String? notes;
  late int userId;
  late int categoryId;
  late int currencyId;

  CategoryActions({
    this.id,
    required this.amount,
    required this.date,
    required this.expense,
    required this.category,
    this.notes,
    required this.userId,
    required this.categoryId,
    required this.currencyId,
  });

  CategoryActions.fromMap(Map<String, dynamic> rowMap) {
    id = rowMap['id'];
    amount = rowMap['amount'];
    date = rowMap['date'];
    expense = rowMap['expense'] == 1 ? true : false;
    category = rowMap['category'];
    notes = rowMap['notes'];
    userId = rowMap['user_id'];
    categoryId = rowMap['category_id'];
    currencyId = rowMap['currency_id'];
  }

  Map<String, dynamic> toMap() {
    Map<String, dynamic> map = Map<String, dynamic>();
    map['amount'] = amount;
    map['date'] = date;
    map['expense'] = expense ? 1 : 0;
    map['category'] = category;
    map['notes'] = notes;
    map['user_id'] = userId;
    map['category_id'] = categoryId;
    map['currency_id'] = currencyId;
    return map;
  }
}
