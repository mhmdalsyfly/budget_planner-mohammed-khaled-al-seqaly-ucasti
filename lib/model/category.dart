import 'package:budget_planner_app/model/db_table.dart';

class Category {
  int? id = 0;
  late int userId;
  late String name;
  late int expense;
  late int countAction = 0;

  static const TABLE_NAME = 'categories';

  Category({
    this.id,
    required this.userId,
    required this.name,
    required this.expense,
  });

  Category.fromMap(Map<String, dynamic> rowMap) {
    id = rowMap['id'];
    userId = rowMap['user_id'];
    name = rowMap['name'];
    expense = rowMap['expense'];
    countAction = rowMap['count_action'];
  }

  Map<String, dynamic> toMap() {
    Map<String, dynamic> map = Map<String, dynamic>();
    map['user_id'] = userId;
    map['name'] = name;
    map['expense'] = expense;
    return map;
  }
}
