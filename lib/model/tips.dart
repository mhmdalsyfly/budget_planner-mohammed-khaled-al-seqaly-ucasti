class Tipss {
  String title;
  String image;
  String date;
  String subject;

  Tipss({
    required this.title,
    required this.image,
    required this.date,
    required this.subject,
  });
}
