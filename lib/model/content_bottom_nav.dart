import 'package:flutter/cupertino.dart';

class ContentBottomNav {
  final String title;
  final Widget screen;

  const ContentBottomNav({
    required this.title,
    required this.screen,
  });
}
