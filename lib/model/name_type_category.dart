class NameTypeCategory {
  int id;
  String type;
  String name;

  NameTypeCategory({
    required this.id,
    required this.name,
    required this.type,
  });
}
