class Currency {
  late int id;
  late String nameEn;
  late String nameAr;

  Currency.fromMap(Map<String, dynamic> rowMap) {
    id = rowMap['id'];
    nameEn = rowMap['name_en'];
    nameAr = rowMap['name_ar'];
  }

  @override
  Map<String, dynamic> toMap() {
    Map<String, dynamic> map = Map<String, dynamic>();
    map['name_en'] = nameEn;
    map['name_ar'] = nameAr;
    return map;
  }
}
