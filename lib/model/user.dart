import 'package:budget_planner_app/model/db_table.dart';

class User {
  int? id = -1;
  late String name;
  late String email;
  late int currencyId;
  late int dayLimit;
  late int pin;

  static const TABLE_NAME = 'users';

  User({
    this.id,
    required this.name,
    required this.email,
    required this.currencyId,
    required this.dayLimit,
    required this.pin,
  });

  User.fromMap(Map<String, dynamic> rowMap) {
    id = rowMap['id'];
    name = rowMap['name'];
    email = rowMap['email'];
    currencyId = rowMap['currency_id'];
    dayLimit = rowMap['day_limit'];
    pin = rowMap['pin'];
  }

  Map<String, dynamic> toMap() {
    Map<String, dynamic> map = Map<String, dynamic>();
    map['name'] = name;
    map['email'] = email;
    map['currency_id'] = currencyId;
    map['day_limit'] = dayLimit;
    map['pin'] = pin;
    return map;
  }
}
