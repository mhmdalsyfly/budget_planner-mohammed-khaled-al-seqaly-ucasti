import 'package:flutter/material.dart';

class AppColors {
  static const Color BG_SCREEN_COLOR = Color(0xffFFFFFF);
  static const Color WHITE = Color(0xffffffff);
  static const Color BLACK = Color(0xFF000000);
  static const Color BG_BUTTON = Color(0xFF472FC8);
  static const Color GREEN = Color(0xFF00BEA1);
  static const Color RICH_TEXT = Color(0xFF351DB6);
  static const Color HINT_TEXT = Color(0xFF9F9DED);
  static const Color TEXT_BOLD = Color(0xFF0D0E56);
  static const Color GRAY = Color(0xFF7B7C98);
  static const Color Label_TextFiled = Color(0xFF181819);
  static const Color BG_BUTTON_GRAY = Color(0xFF979797);
  static const Color BG_BOTTOM_NAVIGATION = Color(0xffF2F2F2);
  static const Color BG_PROGRESS = Color(0xFFF1F4FF);
  static const Color DIVIDER = Color(0xFFD8D8D8);
  static const Color SHADOW = Color(0xffe9e7f1);
  static const Color SHADOW2 = Color(0xff8f8f8f);
  static const Color SHADOW3 = Color(0x34000000);
  static const Color UN_SELECTED_ITEM = Color(0xffD3CFEA);
  static const Color SELECTED_ITEM = Color(0xFF0D0E56);
  static const Color RED = Color(0xFFD02323);
  static const Color RED_TEXT_LISTVIEW = Color(0xFFF33720);
  static const Color RED_ARROW_UP = Color(0xFFF33720);
  static const Color RED_Button_DELETE = Color(0xFFD50000);
  static const Color TEXT_NOTE = Color(0xFFBABAD7);
  static const Color BG_ICON_CLOSE = Color(0xFFB8B9E3);

}
